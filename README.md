## Public ModoK System

## Important

Certain files had to be omitted in this repository, due to potential problems related to web scraping\
As a consequence, the code of this repos is not functional. If you want to have access to the full implementation,
contact the repository owner (lorenzobs@usp.br).

## Repository Organization

[data/](./data/)\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[database_import/](./data/database_import/) - Exports from the current database state\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[db/](./data/db/) - Initial files for the operational database\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[manualData/](./data/manualData/) - Manual data to be added (players and teams)\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[results/](./data/results/) - Formula results\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[superset/](./data/superset/) - Exports from superset (dashboards, charts...)\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[toAdd/](./data/toAdd/) - Manage data to be added\
[src/](./src/)\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[calculation/](./src/calculation/) - Formula calculation\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[extract/](./src/extract/) - Web scraping implementation\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[graph_database/](./src/graph_database/) - Graph-oriented operations\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[interface/](./src/interface/) - CLI and graphical interface\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[op_database/](./src/op_database/) - Document-oriented operations\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[utils/](./src/utils/) - Utils functions for each module\
[tests/](./tests/) - Automated tests\
[./](./) - Other configuration files
## Installation and setup

## Prerequisites

1. Python3 and pip - see https://realpython.com/installing-python/
2. MongoDB - see https://www.mongodb.com/docs/manual/installation/
3. An empty MongoDB database for this project - see https://www.mongodb.com/basics/create-database
4. (A Neo4j installation - see https://neo4j.com/docs/operations-manual/current/installation/)
	OR
   (A Neo4j account and a running sandbox - see https://neo4j.com/)
5. Apache Drill - see https://drill.apache.org/download/
6. Apache Superset - see https://superset.apache.org/docs/intro

## Create a .env file with the following variables:
```
	mongodb_uri= ""
	mongodb_db = ""
	
	neo4j_uri = ""
	neo4j_user = ""
	neo4j_password = ""
	
	SUPERSET_CONFIG_PATH="./superset_config.py"
	SUPERSET_SECRET_KEY=""
	
	NEO4J_CONF=""
	NEO4J_HOME=""
	HEAP_SIZE=4g
```

`mongodb_uri` variable is used to connect to the mongodb database, by default it is "mongodb://127.0.0.1:27017".  
`mongodb_db` is the name of the database created for this project.
`neo4j_uri`, `neo4j_uri` and `neo4j_uri` vriables are used to connect to the neo4j database. It is possible to get all of them by accessing `https://sandbox.neo4j.com/` and creating an empty graph database or by installing neo4j locally
`SUPERSET_CONFIG_PATH`  and `SUPERSET_SECRET_KEY` are necessary to initialize Apache Superset correctly.
`NEO4J_CONF` and `NEO4J_HOME` are used if Neo4j is installed locally.
`HEAP_SIZE` is passed to the neo4j configuration

### Installing venv:
```
pip install virtualenv
```
### Create the virtual environment:
```
python -m venv venv
```
### Edit the activate file:

Firstly, it is necessary to know the absolute path of the .env file 

```
echo $(pwd)/.env
```
Then, edit the ./venv/bin/activate file with this two commands:

In the beggining of the script, add this line after the deactivate function definition:
```
unset $(grep -v '^#' {ENV PATH} | sed -E 's/(.*)=.*/\1/' | xargs)
```
```
deactivate () {
    
    unset $(grep -v '^#' {ENV PATH} | sed -E 's/(.*)=.*/\1/' | xargs)
    
    # reset old environment variables
    if [ -n "${_OLD_VIRTUAL_PATH:-}" ] ; then
    ...
```

In the last line of the script, include:

```
set -o allexport && source {ENV PATH} && set +o allexport
```

### Finally activate the env
```
. venv/bin/activate
```
To deactivate, simply run `deactivate` in the command line.
### To install all requirements inside the environment, run:
```
pip install -r requirements.txt
```

## Starting mongodb

It is possible that mongod is not active in the system. If that's the case, run:
```sudo systemctl start mongod ```

## Starting Apache Drill

`apache-drill-1.21.1/apache-drill-1.21.1/bin/drill-embedded --config sistemamodok/`

## Configuring Apache Drill with MongoDB

To enable MongoDB plugin inside Apache Drill, see - https://drill.apache.org/docs/mongodb-storage-plugin/#configuring-mongodb. 
Then, go to Options and search for "mongo". The values of each option must be as following:
```
store.mongo.all_text_mode = false
store.mongo.bson.record.reader = false
store.mongo.read_numbers_as_double = true
```
## Configuring Apache Superset

First, initialize the database, create an admin user and run Superset:
```
superset db upgrade
superset fab create-admin
superset init
flask --app src/interface/modok-interface.py run
```
Then, to create a connection to Apache Drill, go to Settings -> Database Connections -> Database -> Other and input the following value as SQLAlchemy URI.

`drill+sadrill://localhost:8047/dfs?use_ssl=False`

## Starting Neo4J

`neo4j restart`

For more information, check the `README.md` inside `src/graph_database/`