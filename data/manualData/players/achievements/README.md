# Specifications

## File type

The file must be a CSV, with comma (`,`) as the separator

## File name

The file must be named `id.csv`, where id is the player id
Example: 28003.csv or 523.csv

## File content

The files can contain more columns, but none of the extra ones will be used.

4 columns:
`action` = "Remove" or "Add"
`season` = Season of the achievement
`clubId` = Club Id
`achievement` = Achievement name

### Example
File 28003.csv:
|action|season|clubId|achievement|
|------|------|------|-----------|
|Remove|2022|3437|World Cup winner|
|Add|2022|131|Spanish Champion|
|Remove|2004|131|Spanish Champion|