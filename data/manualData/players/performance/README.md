# Specifications

## File type

The file must be a CSV, with comma (`,`) as the separator

## File name

There are two ways of naming a player's performance file (id should be replaced with the player's id)
`id_matches.csv` if each line represents a match in the player carreer
`id_champs.csv` if each line represents a championship in the player carrer

Example: 28003\_matches.csv or 523\_champs.csv

If both of them are present, the matches file will be parsed first, so in the presence of conflicts, champs will overwrite matches modifications.

## File content 

The files can contain more columns, but none of the extra ones will be used

### Matches


8 columns:
`date` = Date of the match (DD/MM/YYYY)
`champId` = Id of the championship
`clubId` = Club Id
`scored goals` = Gols scored by the team
`conceded goals` = Gols conceded by the team  
`goals` = Total goals
`assists` = Total assists
`saved penalties`= Total penalties saved (goalkeepers only)

### Example
File 28003_matches.csv:
|date|champId|clubId|scored goals|conceded goals|goals|assists|saved penalties|
|----|-------|------|------------|--------------|-----|-------|---------------|
|12/05/2022|CL|131|5|0|3|1|0|0|


### Championships

11 columns:
`champId` = Id of the championhsip
`season` = Season of the achievement
`clubId` = Club Id
`matches` = Total matches
`goals` = Total goals
`assists` = Total assists
`hat-tricks` = Total hat-tricks
`clean sheets`= Total matches without suffering a goal (goalkeepers and defenders only)
`saved penalties`= Total penalties saved (goalkeepers only)
`PPG` = Points per game (points/matches)

### Example
File 28003_champs.csv:

|champId|season|clubId|matches|goals|assists|hat-tricks|clean sheets|saved penalties|PPG|
|-------|------|------|-------|-----|-------|----------|------------|---------------|---|
|CL|2022|131|2|0|1|0|0|0|3|
|WM22|2022|3437|7|7|3|0|0|0|2.75|



## Important

If the manual data conflicts with the scraped data, it will overwrite it. If you need to complement the information present in the scraping, you
have to copy each of the correct info to the adition.

Example: Supose I want to add 1 more goal to the 15 goals from the championship CL in 2022. The champs file must be like this:

|champId|season|clubId|matches|goals|assists|hat-tricks|clean sheets|saved penalties|PPG|
|-------|------|-------|------|-----|-------|----------|------------|---------------|---|
|CL|2022|131|7|16|4|0|0|0|2.5|

If I the csv had only the goal missing, it would overwrite the entire info with 0 matches, 1 goal, 0 assists, etc.

