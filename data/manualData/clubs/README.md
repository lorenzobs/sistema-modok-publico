# Specifications

## File type

The file must be a CSV, with comma (`,`) as the separator

## File name

The file must be named `id.csv`, where id is the club id
Example: 131.csv or 199.csv

## File content

The files can contain more columns, but none of the extra ones will be used.

3 columns:
`action` = "Remove" or "Add"
`season` = Season of the achievement
`achievement` = Achievement name

### Example
File 131.csv:
| action|season|achievement|
|---------|------|--------|
|Remove|2011|Champions League winner|
|Add|2002|Spanish Champion|
|Add|2003|FIFA Club World Cup winner|
|Remove|2014|Spanish Cup winner|
