from flask import Flask, redirect

from flask_appbuilder import expose, IndexView

class SupersetDashboardIndexView(IndexView):
    @expose("/")
    def index(self):
        return redirect("/modok/")

FAB_INDEX_VIEW = f"{SupersetDashboardIndexView.__module__}.{SupersetDashboardIndexView.__name__}"

# Superset specific config
ROW_LIMIT = 5000

# # Flask-WTF flag for CSRF
WTF_CSRF_ENABLED = False
# # Add endpoints that need to be exempt from CSRF protection
# WTF_CSRF_EXEMPT_LIST = []
# # A CSRF token that expires in 1 year
# WTF_CSRF_TIME_LIMIT = 60 * 60 * 24 * 365
FEATURE_FLAGS = {
    "ENABLE_TEMPLATE_PROCESSING":True
}

