'''
	Tests for basic database operations implemented at /src/op_database/operations.py
'''
import pymongo
from pathlib import Path
import sys

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from src.op_database import operations

def connect_database():
	'''
		Custom database connection for tests
	'''
	CONNECTION_STRING = "mongodb://127.0.0.1:27017"
	client = pymongo.MongoClient(CONNECTION_STRING)
	db = client["test_modok"]
	return db

def test_insertion():
	db = connect_database()
	operations.delete_database(db)

	operations.insert_document(db,"COUNTRY",{"_id":"test_insertion_1","name":"test_insertion"})
	items = db["COUNTRY"].find()
	assert len(list(items)) == 1

	operations.insert_document(db,"COUNTRY",{"_id":"test_insertion_2","name":"test_insertion"})
	items = db["COUNTRY"].find()
	assert items[0]["_id"] == "test_insertion_1" 
	assert items[1]["_id"] == "test_insertion_2" 
	assert len(list(items)) == 2
	return

def test_remotion():
	db = connect_database()
	operations.delete_database(db)

	d1 = {"_id":"test_remotion_1","name":"test_remotion"}
	d2 = {"_id":"test_remotion_2","name":"test_remotion"}
	d3 = {"_id":"test_remotion_3","name":"test_remotion"}

	operations.insert_document(db,"COUNTRY",d1)
	operations.insert_document(db,"COUNTRY",d2)
	operations.insert_document(db,"COUNTRY",d3)

	operations.delete_document(db,"COUNTRY",d2)

	items = db["COUNTRY"].find()
	assert items[0] == d1
	assert items[1] == d3
	assert len(list(items)) == 2

	operations.insert_document(db,"COUNTRY",d2)

	items = db["COUNTRY"].find()
	assert items[2] == d2
	assert len(list(items)) == 3
	return

def test_replace():
	db = connect_database()
	operations.delete_database(db)

	d1 = {"_id":"test_update_1","name":"test_update_wrong"}
	new_d1 = {"_id":"test_update_1","name":"test_update_correct"}

	operations.insert_document(db,"COUNTRY",d1)

	query = db["COUNTRY"].find({"name":{ "$regex": "test_update_wrong"}})
	assert len(list(query)) == 1
	query = db["COUNTRY"].find({"name":{ "$regex": "test_update_correct"}})
	assert (len(list(query))) == 0

	operations.replace_document(db,"COUNTRY",d1,new_d1)

	query = db["COUNTRY"].find({"name":{ "$regex": "test_update_wrong"}})
	assert len(list(query)) == 0
	query = db["COUNTRY"].find({"name":{ "$regex": "test_update_correct"}})
	assert (len(list(query))) == 1

	return

def test_get_document_by_att():
	db = connect_database()
	operations.delete_database(db)
	d1 = {"_id":"test_get_1","name":"test_get"}

	operations.insert_document(db,"COUNTRY",d1)

	doc = operations.get_document_by_att("COUNTRY","test_get_1",att="_id",db=db)
	assert doc == d1

	doc = operations.get_document_by_att("COUNTRY","test_get",att="name",db=db)
	assert doc == d1
	
	return

def test_delete_database():
	db = connect_database()

	d1 = {"_id":"test_delete_datbase_1","name":"test_delete_database_1"}
	operations.insert_document(db,"COUNTRY",d1)		
	d2 = {"_id":"test_delete_database_2","name":"test_delete_database_2"}
	operations.insert_document(db,"CONTINENT",d2)

	operations.delete_database(db)

	assert len(list(db["COUNTRY"].find())) == 0
	assert len(list(db["CONTINENT"].find())) == 0
	assert len(list(db["CHAMPIONSHIP"].find())) == 0
	assert len(list(db["PLAYER"].find())) == 0
	assert len(list(db["TEAM"].find())) == 0

def test_delete_collection():
	db = connect_database()

	d1 = {"_id":"test_delete_collection_1","name":"test_delete_collection_1"}
	operations.insert_document(db,"COUNTRY",d1)		
	d2 = {"_id":"test_delete_collection_2","name":"test_delete_collection_2"}
	operations.insert_document(db,"CONTINENT",d2)

	operations.delete_collection(db,"COUNTRY")

	assert len(list(db["COUNTRY"].find())) == 0
	assert len(list(db["CONTINENT"].find())) == 1
	return

def test_query_collection():
	db = connect_database()

	i1 = {"_id":"test_query_collection_1","name":"test_query_collection_1","attribute1":"value1","attribute2":"value2"}
	operations.insert_document(db,"CHAMPIONSHIP",i1)

	query = {"filter":{"_id":"test_query_collection_1"}}
	response = operations.query_collection("CHAMPIONSHIP",query,db=db)
	assert response[0] == i1

	query = {"filter":{"_id":"test_query_collection_1"}}
	response = operations.query_collection("CHAMPIONSHIP",query,db=db)
	assert response[0] == i1

	query = {"filter":{"_id":"test_query_collection_1"},"projection":{"_id":0}}
	response = operations.query_collection("CHAMPIONSHIP",query,db=db)
	assert response[0] == {"name":i1["name"],"attribute1":i1["attribute1"],"attribute2":i1["attribute2"]}	



	return

