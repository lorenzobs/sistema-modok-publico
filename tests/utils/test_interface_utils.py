'''
	Tests for interface utils
'''
from pathlib import Path
import sys

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from src.utils import interfaceUtils

def test_convert_seconds():
	assert interfaceUtils.convert_seconds(0) == (0,0,0)
	assert interfaceUtils.convert_seconds(1) == (0,0,1)
	assert interfaceUtils.convert_seconds(35) == (0,0,35)
	assert interfaceUtils.convert_seconds(60) == (0,1,0)
	assert interfaceUtils.convert_seconds(67) == (0,1,7)
	assert interfaceUtils.convert_seconds(465) == (0,7,45)
	assert interfaceUtils.convert_seconds(3600) == (1,0,0)
	assert interfaceUtils.convert_seconds(3599) == (0,59,59)
	assert interfaceUtils.convert_seconds(18000) == (5,0,0)
	assert interfaceUtils.convert_seconds(18465) == (5,7,45)
