'''
	Tests for general utils
'''
from pathlib import Path
import sys
import numpy

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from src.utils import utils

def test_convert_season():
	assert utils.convert_season("20/21") == 2020
	assert utils.convert_season("1920") == 1920
	assert utils.convert_season(1920) == 1920
	assert utils.convert_season("2003/04") == 2003
	assert utils.convert_season("2003/2004") == 2003
	assert utils.convert_season("99/00") == 1999
	assert utils.convert_season("22/23") == 2022
	assert utils.convert_season("70/71") == 1970
	assert utils.convert_season(numpy.int64(1966)) == 1966
	return

def test_date_to_season():
	assert utils.date_to_season("20/04/1977") == 1976
	assert utils.date_to_season("20/08/1977") == 1977
	assert utils.date_to_season("31/06/1977") == 1976
	assert utils.date_to_season("01/07/1977") == 1977
	assert utils.date_to_season("20/04/1977",typeOfSeason="South") == 1977
	assert utils.date_to_season("20/08/1977",typeOfSeason="South") == 1977
	assert utils.date_to_season("31/06/1977",typeOfSeason="South") == 1977
	assert utils.date_to_season("01/07/1977",typeOfSeason="South") == 1977
	return

def test_sanitize_championship():
	assert utils.sanitize_championship("Copa América Champion") == "copaamericachampion"
	assert utils.sanitize_championship("uefa cup-champion") == "uefacupchampion"
	return