from pathlib import Path
import sys

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from src.utils import extractUtils


def test_date_conversion():
	assert extractUtils.date_conversion("Jan 12, 2021") == "12/01/2021"
	assert extractUtils.date_conversion("Feb 06, 2021") == "06/02/2021"
	assert extractUtils.date_conversion("Mar 24, 2021") == "24/03/2021"
	assert extractUtils.date_conversion("Apr 30, 2021") == "30/04/2021"
	assert extractUtils.date_conversion("May 19, 2021") == "19/05/2021"
	assert extractUtils.date_conversion("Jun 01, 2021") == "01/06/2021"
	assert extractUtils.date_conversion("Jul 29, 2021") == "29/07/2021"
	assert extractUtils.date_conversion("Aug 10, 2021") == "10/08/2021"
	assert extractUtils.date_conversion("Sep 18, 2021") == "18/09/2021"
	assert extractUtils.date_conversion("Oct 23, 2021") == "23/10/2021"
	assert extractUtils.date_conversion("Nov 29, 2002") == "29/11/2002"
	assert extractUtils.date_conversion("Dec 31, 1987") == "31/12/1987"

def test_typeOfSeason():
	assert extractUtils.get_typeOfSeason("CL") == "North"
	assert extractUtils.get_typeOfSeason("BRA1") == "South"
	assert extractUtils.get_typeOfSeason("CA16") == "South"
	assert extractUtils.get_typeOfSeason("WM22") == "South"
	assert extractUtils.get_typeOfSeason("KLUB") == "North"
	assert extractUtils.get_typeOfSeason("ES1") == "North"

def test_nationalities():
	assert extractUtils.nationalities[""] == ""
	assert extractUtils.nationalities["Argentina"] == 3437
	assert extractUtils.nationalities["Brazil"] == 3439
	assert extractUtils.nationalities["Romania"] == 3447