'''
	Tests for the calculate utils functions
'''
from pathlib import Path
import sys

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from src.utils import calculateUtils


def test_calculate_full_ppg():
	mock_perf = {"2022":{"Matches":0,"Points":0}}
	assert calculateUtils.calculate_full_ppg(mock_perf) == {"2022":0,"All":0} 
	mock_perf["2022"]["Matches"] = 10
	assert calculateUtils.calculate_full_ppg(mock_perf) == {"2022":0,"All":0} 

	mock_perf = {"2022":{"Matches":15,"Points":18.25},
				"2021":{"Matches":46,"Points":61.36},
				"2020":{"Matches":26,"Points":43.77},
				"2019":{"Matches":30,"Points":60.85},
				}
	assert calculateUtils.calculate_full_ppg(mock_perf) == {"2022":0.40555555555555556,"2021":0.44463768115942026,"2020":0.5611538461538462,"2019":0.6761111111111111,"All":0.5248717948717948} 
	return

def test_find_champ_weight():
	assert calculateUtils.find_champ_weight("BRA1") == {"champ_id":"BRA1","champ":"BRA1","weight":2.0,"type":"Id","country":"Brazil","default_weight":2.0}
	assert calculateUtils.find_champ_weight("CL") == {"champ_id":"CL","champ":"CL","weight":4.75,"type":"Id","country":"Continental","default_weight":4.75}
	assert calculateUtils.find_champ_weight("champions league winner") == {"champ_id":"CL","champ":"champions league winner","weight":47.5,"type":"Champion","country":"Continental","default_weight":4.75}
	return

def test_find_country_season_weight():
	assert calculateUtils.find_country_season_weight("Brazil",1968) == {"country":"Brazil","weight":0.49,"season":1968,"continent":"South America"}
	assert calculateUtils.find_country_season_weight("World",1967) == {"country":"World","weight":1,"season":1967,"continent":None}
	assert calculateUtils.find_country_season_weight("National Continental",2002) == {"country":"National Continental","weight":1,"season":2002,"continent":None}
	assert calculateUtils.find_country_season_weight("Friendly",2021) == {"country":"Friendly","weight":1,"season":2021,"continent":None}
	return

def test_find_continent_season_weight():
	assert calculateUtils.find_continent_season_weight("South America",2006,"National Continental") == {"continent":"South America","season":2006,"key":"national_weights","weight":1}
	assert calculateUtils.find_continent_season_weight("Europe",1958,"Spain") == {"continent":"Europe","season":1958,"key":"club_weights","weight":0.99}
	assert calculateUtils.find_continent_season_weight("Asia",1995,"Friendly") == {"continent":"Asia","season":1995,"key":None,"weight":1}
	return

def test_find_continent_from_team():
	assert calculateUtils.find_continent_from_team(131) == "Europe"
	assert calculateUtils.find_continent_from_team(3437) == "South America"

def test_find_country_from_team():
	assert calculateUtils.find_country_from_team(131) == "Spain"
	assert calculateUtils.find_country_from_team(3437) == "Argentina"



def test_find_first_season_team():

	teamsHePlayedBySeason = {"Teams":[1],"Seasons":{"2022":[1]}}
	assert int(calculateUtils.find_first_season_team(1,2022,teamsHePlayedBySeason)) == 2022
	assert int(calculateUtils.find_first_season_team(1,2022,teamsHePlayedBySeason,isNationalTeam=True)) == 2022
	teamsHePlayedBySeason = {"Teams":[1],"Seasons":{"2022":[1],"2021":[1],"2020":[1],"2019":[1]}}
	assert int(calculateUtils.find_first_season_team(1,2022,teamsHePlayedBySeason)) == 2019
	assert int(calculateUtils.find_first_season_team(1,2022,teamsHePlayedBySeason,isNationalTeam=True)) == 2019
	assert int(calculateUtils.find_first_season_team(1,2020,teamsHePlayedBySeason)) == 2019
	assert int(calculateUtils.find_first_season_team(1,2020,teamsHePlayedBySeason,isNationalTeam=True)) == 2019
	assert int(calculateUtils.find_first_season_team(1,2019,teamsHePlayedBySeason)) == 2019
	assert int(calculateUtils.find_first_season_team(1,2019,teamsHePlayedBySeason,isNationalTeam=True)) == 2019
	teamsHePlayedBySeason = {"Teams":[1],"Seasons":{"2022":[1],"2021":[1],"2020":[],"2019":[1],"2018":[1]}}
	assert int(calculateUtils.find_first_season_team(1,2022,teamsHePlayedBySeason)) == 2021
	assert int(calculateUtils.find_first_season_team(1,2022,teamsHePlayedBySeason,isNationalTeam=True)) == 2018
	assert int(calculateUtils.find_first_season_team(1,2019,teamsHePlayedBySeason)) == 2018
	assert int(calculateUtils.find_first_season_team(1,2019,teamsHePlayedBySeason,isNationalTeam=True)) == 2018
	teamsHePlayedBySeason = {"Teams":[1],"Seasons":{"2022":[1],"2021":[0],"2020":[1],"2019":[1],"2018":[1]}}
	assert int(calculateUtils.find_first_season_team(1,2022,teamsHePlayedBySeason)) == 2022
	assert int(calculateUtils.find_first_season_team(1,2022,teamsHePlayedBySeason,isNationalTeam=True)) == 2018
	assert int(calculateUtils.find_first_season_team(1,2019,teamsHePlayedBySeason)) == 2018
	assert int(calculateUtils.find_first_season_team(1,2019,teamsHePlayedBySeason,isNationalTeam=True)) == 2018

def test_find_last_season_team():
	teamsHePlayedBySeason = {"Teams":[1],"Seasons":{"2022":[1]}}
	assert int(calculateUtils.find_last_season_team(1,2022,teamsHePlayedBySeason)) == 2022
	assert int(calculateUtils.find_last_season_team(1,2022,teamsHePlayedBySeason,isNationalTeam=True)) == 2022
	teamsHePlayedBySeason = {"Teams":[1],"Seasons":{"2022":[1],"2021":[1],"2020":[1],"2019":[1]}}
	assert int(calculateUtils.find_last_season_team(1,2022,teamsHePlayedBySeason)) == 2022
	assert int(calculateUtils.find_last_season_team(1,2022,teamsHePlayedBySeason,isNationalTeam=True)) == 2022
	assert int(calculateUtils.find_last_season_team(1,2020,teamsHePlayedBySeason)) == 2022
	assert int(calculateUtils.find_last_season_team(1,2020,teamsHePlayedBySeason,isNationalTeam=True)) == 2022
	assert int(calculateUtils.find_last_season_team(1,2019,teamsHePlayedBySeason)) == 2022
	assert int(calculateUtils.find_last_season_team(1,2019,teamsHePlayedBySeason,isNationalTeam=True)) == 2022
	teamsHePlayedBySeason = {"Teams":[1],"Seasons":{"2022":[1],"2021":[1],"2020":[],"2019":[1],"2018":[1]}}
	assert int(calculateUtils.find_last_season_team(1,2022,teamsHePlayedBySeason)) == 2022
	assert int(calculateUtils.find_last_season_team(1,2022,teamsHePlayedBySeason,isNationalTeam=True)) == 2022
	assert int(calculateUtils.find_last_season_team(1,2019,teamsHePlayedBySeason)) == 2019
	assert int(calculateUtils.find_last_season_team(1,2019,teamsHePlayedBySeason,isNationalTeam=True)) == 2022
	teamsHePlayedBySeason = {"Teams":[1],"Seasons":{"2022":[1],"2021":[0],"2020":[1],"2019":[1],"2018":[1]}}
	assert int(calculateUtils.find_last_season_team(1,2022,teamsHePlayedBySeason)) == 2022
	assert int(calculateUtils.find_last_season_team(1,2022,teamsHePlayedBySeason,isNationalTeam=True)) == 2022
	assert int(calculateUtils.find_last_season_team(1,2019,teamsHePlayedBySeason)) == 2020
	assert int(calculateUtils.find_last_season_team(1,2019,teamsHePlayedBySeason,isNationalTeam=True)) == 2022
