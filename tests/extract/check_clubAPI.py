from bs4 import BeautifulSoup

def check_url_info(url,soup,idClub):
	exception = Exception("Invalid url for clubId = " + str(idClub)+":\n\t" + url)
	try:
		if not soup.find(class_='data-header').find(class_='data-header__headline-container'):
			raise exception
		if not soup.find(class_='profilheader'):
			raise exception
	except:
		raise exception
	return

def check_url_achievements(url,soup,idClub):
	exception = Exception("Invalid url for clubId = " + str(idClub)+":\n\t" + url)
	try:
		if not soup.find(class_='large-4 columns'):
			raise exception
	except:
		raise exception
	return