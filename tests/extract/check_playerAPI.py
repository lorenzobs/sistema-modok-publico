from bs4 import BeautifulSoup


def check_url_info(url,soup,idPlayer):
	exception = Exception("Invalid url for playerId = " + str(idPlayer)+":\n\t" + url)
	try:
		dH = soup.find(class_='data-header')
		if not dH:
			raise exception
		if not dH.find(class_='data-header__headline-wrapper'):
			raise exception
		if not dH.find(class_='data-header__content',itemprop='birthDate'):
			raise exception
		if not dH.find_all("li",class_='data-header__label'):
			raise exception
		if not dH.find(class_='data-header__content',itemprop='nationality'):
			raise exception
	except:
		raise exception
	return

def check_url_transfers(url,soup,idPlayer):
	exception = Exception("Invalid url for playerId = " + str(idPlayer)+":\n\t" + url)	
	try:
		if not soup.find(class_='box viewport-tracking').find_all(class_='grid tm-player-transfer-history-grid'):
			raise exception
	except:
		raise exception
	return

def check_url_achievements(url,soup,idPlayer):
	exception = Exception("Invalid url for playerId = " + str(idPlayer)+":\n\t" + url)
	try:
		if not soup.find(class_='large-4 columns'):
			raise exception
	except:
		raise exception
	return

def check_url_hattricks(url,soup,idPlayer):
	exception = Exception("Invalid url for playerId = " + str(idPlayer)+":\n\t" + url)
	try:
		if not soup.find(class_='large-8 columns').find(class_='responsive-table'):
			raise exception
	except:
		raise exception
	return

def check_url_penalties(url,soup,idPlayer):
	exception = Exception("Invalid url for playerId = " + str(idPlayer)+":\n\t" + url)
	try:
		rT = soup.find(class_='large-8 columns').find(class_='responsive-table')
		if not rT:
			raise exception
	except:
		raise exception
	return	

def check_url_club_performance(url,soup,idPlayer):
	exception = Exception("Invalid url for playerId = " + str(idPlayer)+":\n\t" + url)
	try:
		if not soup.find(lambda tag: tag.name == 'div' and tag.get('class') == ['row']).find(class_='large-12 columns').find('table'):
			raise exception
		if not soup.find(class_='box').find(class_='responsive-table').find(class_='items').find('tbody').find_all('tr'):
			raise exception
	except:
		raise exception

def check_url_match_info(url,soup,idPlayer):
	exception = Exception("Invalid url for playerId = " + str(idPlayer)+":\n\t" + url)
	try:
		if not soup.find_all('h2',class_='content-box-headline'):
			raise exception
	except:
		raise exception
	return

def check_url_gwsg(url,soup,idPlayer):
	exception = Exception("Invalid url for playerId = " + str(idPlayer)+":\n\t" + url)
	try:
		if len(soup.find(lambda tag: tag.name == 'div' and tag.get('class') == ['row']).find(class_='large-12 columns').find_all(class_='box')) < 2:
			raise exception
	except:
		raise exception
	return

def check_url_national_performance(url,soup,idPlayer):
	exception = Exception("Invalid url for playerId = " + str(idPlayer)+":\n\t" + url)
	try:
		if not soup.find(lambda tag: tag.get('class') == ['row']):
			raise exception
	except:
		raise exception
	return