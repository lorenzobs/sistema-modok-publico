'''
	Responsible for calling the insertion and calculation methods
'''
import json
from multiprocessing import Process
from pathlib import Path
import sys

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))
from src.op_database import insertExtractions
from src.op_database.connect import connect_database
from src.calculation import calculate
from src.calculation import manageToAdd

from src.graph_database.operations import update_node_player,update_edges_player

class InsertAndCalculate():
	def __init__(self,idPlayer,db="",debug=False):

		if db == "":
			db = connect_database()

		self.db = db
		self.idPlayer = idPlayer
		self.debug = debug
		self.toAdd = {}
		return

	def insertion(self):
		insertExtractions.insert_player(self.idPlayer,db=self.db,debug=self.debug)
		return

	def calculation(self):
		c = calculate.Calculate(self.idPlayer,db=self.db,debug=self.debug)
		return

	def insert_and_calculate(self):
		self.insertion()
		self.calculation()
		return 


def insert_and_calculate(id_player,db="",debug=False,callbackProgress=None):
	worked = True
	try:
		module = InsertAndCalculate(id_player,db=db,debug=debug)
		module.insert_and_calculate()
		
		update_node_player(id_player)

		worked = True
	except Exception as error:
		worked = False
		print(error)
	if callbackProgress is not None:
		if worked:
			callbackProgress(id_player,True)
		else:
			callbackProgress(id_player,False)
	return

def insert_and_calculate_multiple(player_list,debug=False,callbackProgress=None):
	processes = []
	db = connect_database()
	for p in player_list:
		process = Process(target=insert_and_calculate,args=[p],kwargs={"db":db,"debug":debug,"callbackProgress":callbackProgress})
		processes.append(process)
		process.start()

	for process in processes:
		if process.is_alive():
			process.join()
	return	