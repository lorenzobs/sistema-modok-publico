'''
	Responsible for calculating the score of a player
'''
import json
from pathlib import Path
import sys

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))
from src.op_database import insertScore
from src.op_database import connect
from src.calculation import manageToAdd
from src.utils import calculateUtils

class Calculate:
	def __init__(self,idPlayer,db="",debug=False):
		if db == "":
			db = connect.connect_database()

		self.db = db
		self.idPlayer = idPlayer
		self.debug = debug
		self.toAdd = {"Teams":{},"Championships":{}}

		self.score_dict = {"Total":0,
						   "c1":{"Total":0,
						   		 "Seasons":{}
						   		 },
						   "c2":{"Total":0,
						   		 "Seasons":{}
						   		 },
						   "c3":{"Total":0,
						   		 "Seasons":{}
						   		 },
						   "Seasons":{},
						   "Clubs":{},
						   "National":{}
						   }

		self.playerData = calculateUtils.get_player_data(idPlayer,db=self.db)
		if self.playerData is None:
			raise Exception("Player " + str(idPlayer) + " not in database")
		self.teamsHePlayedData,self.toAdd["Teams"] = calculateUtils.get_teams_data(self.playerData["Teams"],self.idPlayer,db=self.db)

		# created to avoid repeated queries at the db
		self.champsCache = {}
		self.countryCache = {}
		self.continentCache = {}

		self.full_ppg = {}

		self.individualComponent = self.component_1()
		self.achievementsComponent = self.component_2()
		self.impactComponent = self.component_3()

		self.resultantComponent = self.join_components(self.individualComponent,self.achievementsComponent,self.impactComponent)
		self.insert_score()
		
		manager = manageToAdd.toAddManager()
		manager.new_addition(self.idPlayer,self.toAdd)
		print("Player " + str(self.idPlayer) + " score was successfully calculated" )
		return

	def component_1(self):
		'''
			Calculates a score based on individual performance of a player
		'''
		c1 = {"Total":0,"Seasons":{},"NationalTeam":{"Total":0,"Seasons":{}},"Clubs":{"Total":0,"Seasons":{}}}
		performance = self.playerData["Performance"]
		for season in performance:
			for champ in performance[season]:
				champ_weight = self.find_champ_weight(champ,season=season)

				if (champ_weight["weight"] == 0):
					self.add_to_add_champ(champ,"Id")
					continue

				country_weight = self.find_country_season_weight(champ_weight["country"],season)

				for team in performance[season][champ]:
					if (not self.isValidTeam(team)): continue
					
					continent_weight = self.find_continent_season_weight(country_weight["continent"],season,champ_weight["country"],team)

					stats = performance[season][champ][team]
					matches = stats["Matches"]
					goals = stats["Goals"]
					assists = stats["Assists"]
					hattricks = stats["Hat-tricks"]
					ppg = stats["PPG"]
					savedPen = stats["SavedPenalties"]
					cleanSheets = stats["GWSG"]
					
					if self.playerData["position"] == "Goalkeeper":
						cleanSheetsWeight = calculateUtils.stats_weights["gwsg"]["Goalkeeper"]
					else:
						cleanSheetsWeight = calculateUtils.stats_weights["gwsg"]["Defender"]

					# The actual formula application
					stats_result = calculateUtils.stats_weights["goal"] * goals + \
							 calculateUtils.stats_weights["assist"] * assists + \
							 calculateUtils.stats_weights["hat-trick"] * hattricks + \
							 cleanSheetsWeight * cleanSheets + \
							 calculateUtils.stats_weights["savedPenalties"] * savedPen

					final_result = stats_result * champ_weight["weight"] * country_weight["weight"] * continent_weight["weight"] * ppg/3
					if season not in self.full_ppg:
						self.full_ppg[season] = {"Matches":0,"Points":0}
					self.full_ppg[season]["Matches"] += matches
					self.full_ppg[season]["Points"] += ppg * matches

					if self.isNationalTeam(team):
						key = "NationalTeam"
					else:
						key = "Clubs"
					c1 = self.component_return_dict(c1,season,final_result,key)

		self.full_ppg = calculateUtils.calculate_full_ppg(self.full_ppg)

		return c1

	def component_2(self):
		'''
			Calculates a score based on achievements of a player's carreer
		'''

		def find_number_matches(season,champInfo,teamId):
			'''
				Find how many matches were played in a specific season-championship-team
				by the player
			'''
			teamId = str(teamId)
			champId = champInfo["champ_id"]
			perf = self.playerData["Performance"]
			if season not in perf:
				return 0
			if champId not in perf[season]:
				possible_champ_ids = calculateUtils.find_possible_champ_ids(champInfo["champ"])
				for c in possible_champ_ids:
					if c in perf[season]:
						champId = c
						break
				if champId not in perf[season]:
					if self.debug:
						print("[WARNING - " + str(self.idPlayer) + "] - Calculation:","champion without playing:",champInfo["champ"] + " (" + str(champId) + "|" + str(season) + ")",file=sys.stderr)
					return 0
			if teamId not in perf[season][champId]:
				return 0
			return perf[season][champId][teamId]["Matches"]
		
		def matches_adjustment(season,champ,team):
			'''
				Calculate the adjustment of the portion of matches played in relation to a minimum quantity.
				This is to avoid players with 5 matches out of 38 being reward the same as someone
				who played all championship matches, for example
			'''
			actual_matches = find_number_matches(season,champ,team)
			w = str(champ["default_weight"])
			if w not in calculateUtils.champs_min_matches:
				if self.debug:
					print("[WARNING - " + str(self.idPlayer) + "] - Calculation: Add weight",w,"to calculateUtils.champs_min_matches",file=sys.stderr)
				return 1
			expected_matches = calculateUtils.champs_min_matches[w]
			minimum = min(expected_matches,actual_matches)

			threshold = 0.15
			if expected_matches == 1:
				threshold = 0.5
			if float(w) > 3.5:
				threshold = 0.3
			try:
				adjust = max(threshold,minimum/expected_matches)
			except:
				adjust = 1
			return adjust

		def dict_to_component3(season,team,value):
			if season not in self.scoreAchievementsToComponent3:
				self.scoreAchievementsToComponent3[season] = {}
			if team not in self.scoreAchievementsToComponent3[season]:
				self.scoreAchievementsToComponent3[season][team] = 0
			self.scoreAchievementsToComponent3[season][team] += value
			return

		c2 = {"Total":0,"Seasons":{},"NationalTeam":{"Total":0,"Seasons":{}},"Clubs":{"Total":0,"Seasons":{}}}
		achievements = self.playerData["Achievements"]

		self.scoreAchievementsToComponent3 = {}

		for season in achievements:

			if season not in self.full_ppg:
				if self.debug:
					print("[WARNING - " + str(self.idPlayer) + "] - Calculation: " + str(season) + " not in performance but in achievements",file=sys.stderr)
				usage = 0
			else:
				usage = self.full_ppg[season]

			for champ in achievements[season]:
				champ_weight = self.find_champ_weight(champ,season=season)
				
				if (champ_weight["weight"] == 0):
					self.add_to_add_champ(champ,"champ or runnerup")
					continue

				country_weight = self.find_country_season_weight(champ_weight["country"],season)

				for team_dict in achievements[season][champ]:
					team = team_dict["TeamId"]
					if (not self.isValidTeam(team)): continue

					continent_weight = self.find_continent_season_weight(country_weight["continent"],season,champ_weight["country"],team)

					matches_adjust = matches_adjustment(season,champ_weight,team)

					# The actual formula application
					final_result = champ_weight["weight"] * country_weight["weight"] * continent_weight["weight"] * usage * matches_adjust

					dict_to_component3(season,team,champ_weight["weight"] * country_weight["weight"] * continent_weight["weight"])
					
					if self.isNationalTeam(team):
						key = "NationalTeam"
					else:
						key = "Clubs"

					c2 = self.component_return_dict(c2,season,final_result,key)
		return c2

	def component_3(self):
		'''
			Calculates a score based on the impact of a player in the clubs he played
		'''

		def teamsHePlayedBySeason():
			'''
				Returns a dict with each season as key
				and a list of teams he played this season
				as value
			'''
			teams = {"Teams":[],"Seasons":{}}
			for season in self.playerData["Performance"]:
				teams["Seasons"][season] = []
				for champ in self.playerData["Performance"][season]:
					for team in self.playerData["Performance"][season][champ]:
						if int(team) not in teams["Seasons"][season] and self.isValidTeam(team):
							teams["Seasons"][season].append(int(team))
						if int(team) not in teams["Teams"] and self.isValidTeam(team):
							teams["Teams"].append(int(team))
			for season in self.playerData["Achievements"]:
				if season not in teams:
					teams["Seasons"][season] = []
				for champ in self.playerData["Achievements"][season]:
					for team in self.playerData["Achievements"][season][champ]:
						if int(team["TeamId"]) not in teams["Seasons"][season] and self.isValidTeam(team["TeamId"]):
							teams["Seasons"][season].append(int(team["TeamId"]))
						if int(team["TeamId"]) not in teams["Teams"] and self.isValidTeam(team["TeamId"]):
							teams["Teams"].append(int(team["TeamId"]))
			return teams
		
		def getTeamScore(team):
			'''
				Calculate the score of a team
			'''
			teamScore = {"Total":0,"Seasons":{}}
			teamData = self.teamsHePlayedData[team]
			for season in teamData["Achievements"]:
				teamScore["Seasons"][season] = 0
				for champ in teamData["Achievements"][season]:
					champ_weight = self.find_champ_weight(champ["titleName"],season=season)
					if (champ_weight["weight"] == 0):
						self.add_to_add_champ(champ["titleName"],"champ or runnerup",trigger="Team",team=team)
						continue
					country_weight = self.find_country_season_weight(champ_weight["country"],season)
					continent_weight = self.find_continent_season_weight(country_weight["continent"],season,champ_weight["country"],team)
					result = champ_weight["weight"] * country_weight["weight"] * continent_weight["weight"]
					teamScore["Seasons"][season] += result
					teamScore["Total"] += result
			return teamScore

		def getPlayerScore():
			'''
				Calculate the achievements score of a player
			'''
			playerScore = self.scoreAchievementsToComponent3
			# for season in self.playerData["Achievements"]:
			# 	playerScore[season] = {}
			# 	for champ in self.playerData["Achievements"][season]:
			# 		champ_weight = self.find_champ_weight(champ,season=season)
			# 		if champ_weight["weight"] == 0: continue
			# 		for team in self.playerData["Achievements"][season][champ]:
			# 			if team["TeamId"] not in playerScore[season]:
			# 				playerScore[season][team["TeamId"]] = 0
			# 			playerScore[season][team["TeamId"]] += champ_weight["weight"] 
			return playerScore


		c3 = {"Total":0,"Seasons":{},"NationalTeam":{"Total":0,"Seasons":{}},"Clubs":{"Total":0,"Seasons":{}}}

		# Getting the seasons a player participated
		seasonsPerf = list(dict(sorted(self.playerData["Performance"].items(), key=lambda x:int(x[0]), reverse=True)).keys())
		seasonsAch = list(dict(sorted(self.playerData["Achievements"].items(), key=lambda x:int(x[0]), reverse=True)).keys())
		seasons = sorted(map(int,list(set(seasonsPerf + seasonsAch)))) 

		# Getting teams the player played in each season
		teamsHePlayedBySeason = teamsHePlayedBySeason()

		# Getting teams score
		teamsScores = {}
		for team in teamsHePlayedBySeason["Teams"]:
			teamsScores[team] = getTeamScore(team)

		# Getting player achievement score (without country and continent weights)
		playerScore = getPlayerScore()

		for season in seasons:
			teamsThisSeason = teamsHePlayedBySeason["Seasons"][str(season)]

			if str(season) not in self.full_ppg:
				usage = 0
			else:
				usage = self.full_ppg[str(season)]

			for team in teamsThisSeason:

				try:
					scorePlayerCurrentSeason = playerScore[str(season)][int(team)]
				except:
					scorePlayerCurrentSeason = 0

				# Impact short

				begin = int(calculateUtils.find_first_season_team(team,season,teamsHePlayedBySeason,isNationalTeam=self.isNationalTeam(team)))
				end = int(calculateUtils.find_last_season_team(team,season,teamsHePlayedBySeason,isNationalTeam=self.isNationalTeam(team)))

				qty = end-begin+1

				teamScoreSeasonsCorrespondent = {}
				for s in range(begin-1,begin-qty-1,-1):
					try:
						teamScoreSeasonsCorrespondent[s] = teamsScores[int(team)]["Seasons"][str(s)]
					except:
						teamScoreSeasonsCorrespondent[s] = 0

				teamScoreSeasonsCorrespondent = dict(sorted(teamScoreSeasonsCorrespondent.items(), key=lambda x:float(x[1])))

				playerScoreSeasonsCorrespondent = {}
				for s in range(begin,end+1):
					try:
						playerScoreSeasonsCorrespondent[s] = playerScore[str(s)][int(team)]
					except:
						playerScoreSeasonsCorrespondent[s] = 0
				playerScoreSeasonsCorrespondent = dict(sorted(playerScoreSeasonsCorrespondent.items(), key=lambda x:float(x[1])))

				currentSeasonIdx = list(playerScoreSeasonsCorrespondent.keys()).index(season)
				correspondentSeason = list(teamScoreSeasonsCorrespondent.keys())[currentSeasonIdx]
				scoreTeamCorrespondentSeason = teamScoreSeasonsCorrespondent[correspondentSeason]


				impact_short = (scorePlayerCurrentSeason - scoreTeamCorrespondentSeason)*10


				# Impact long			
				teamScoreAllTime = 0
				for s in teamsScores[team]["Seasons"]:
					if int(s) > int(end):
						continue
					teamScoreAllTime += teamsScores[team]["Seasons"][s]
				if teamScoreAllTime == 0 and scorePlayerCurrentSeason != 0 and self.debug:
					print("[WARNING - " + str(self.idPlayer) + "] - Calculation: player score != 0 but teamScore = 0 " + str(team) + "|" + str(season),file=sys.stderr)

				try:
					impact_long = (scorePlayerCurrentSeason / teamScoreAllTime)*1000
					if teamScoreAllTime == 0:
						impact_long = 0
				except:
					impact_long = 0

				formula = usage*(impact_long+impact_short)/2
				# Not considering negative impacts
				final_result = max(0,formula)
				teamKey = "Clubs"
				if self.isNationalTeam(team):
					teamKey = "NationalTeam"

				c3 = self.component_return_dict(c3,season,final_result,teamKey)


		return c3

	def join_components(self,c1,c2,c3):
		resultant = {"Total":0,"C1":0,"C2":0,"C3":0,"Seasons":{},
					"NationalTeam":{"Total":0,"C1":0,"C2":0,"C3":0,"Seasons":{}},
					"Clubs":{"Total":0,"C1":0,"C2":0,"C3":0,"Seasons":{}}}

		resultant["Total"] = c1["Total"] + c2["Total"] + c3["Total"]
		resultant["C1"] = c1["Total"]
		resultant["C2"] = c2["Total"]
		resultant["C3"] = c3["Total"]
		resultant["NationalTeam"]["Total"] = c1["NationalTeam"]["Total"] + c2["NationalTeam"]["Total"] + c3["NationalTeam"]["Total"] 
		resultant["NationalTeam"]["C1"] = c1["NationalTeam"]["Total"]
		resultant["NationalTeam"]["C2"] = c2["NationalTeam"]["Total"]
		resultant["NationalTeam"]["C3"] = c3["NationalTeam"]["Total"]
		resultant["Clubs"]["Total"] = c1["Clubs"]["Total"] + c2["Clubs"]["Total"] + c3["Clubs"]["Total"]
		resultant["Clubs"]["C1"] = c1["Clubs"]["Total"]
		resultant["Clubs"]["C2"] = c2["Clubs"]["Total"]
		resultant["Clubs"]["C3"] = c3["Clubs"]["Total"]

		for component,name in [[c1,"C1"],[c2,"C2"],[c3,"C3"]]:

			for season in component["Seasons"]:
				
				if season not in resultant["Seasons"]:
					resultant["Seasons"][season] = {}
					resultant["Seasons"][season]["Total"] = 0
					resultant["Seasons"][season]["C1"] = 0
					resultant["Seasons"][season]["C2"] = 0
					resultant["Seasons"][season]["C3"] = 0

				resultant["Seasons"][season]["Total"] += component["Seasons"][season]
				resultant["Seasons"][season][name] += component["Seasons"][season]

			for season in component["NationalTeam"]["Seasons"]:
				
				if season not in resultant["NationalTeam"]["Seasons"]:
					resultant["NationalTeam"]["Seasons"][season] = {}
					resultant["NationalTeam"]["Seasons"][season]["Total"] = 0
					resultant["NationalTeam"]["Seasons"][season]["C1"] = 0
					resultant["NationalTeam"]["Seasons"][season]["C2"] = 0
					resultant["NationalTeam"]["Seasons"][season]["C3"] = 0

				resultant["NationalTeam"]["Seasons"][season]["Total"] += component["NationalTeam"]["Seasons"][season]
				resultant["NationalTeam"]["Seasons"][season][name] += component["NationalTeam"]["Seasons"][season]

			for season in component["Clubs"]["Seasons"]:
				
				if season not in resultant["Clubs"]["Seasons"]:
					resultant["Clubs"]["Seasons"][season] = {}
					resultant["Clubs"]["Seasons"][season]["Total"] = 0
					resultant["Clubs"]["Seasons"][season]["C1"] = 0
					resultant["Clubs"]["Seasons"][season]["C2"] = 0
					resultant["Clubs"]["Seasons"][season]["C3"] = 0

				resultant["Clubs"]["Seasons"][season]["Total"] += component["Clubs"]["Seasons"][season]
				resultant["Clubs"]["Seasons"][season][name] += component["Clubs"]["Seasons"][season]

		fixed_precision = 2

		resultant = self.round_dict(fixed_precision,resultant)

		return resultant

	def round_dict(self,precision,dic):
		for key in dic:
			if isinstance(dic[key],dict):
				dic[key] = self.round_dict(precision,dic[key])
			else:
				dic[key] = round(dic[key],precision)
		return dic

	def component_return_dict(self,cdict,season,final_result,key):
		'''
			Organizes the return structure by season and
			national team/clubs
		'''
		season = str(season)
		cdict["Total"] += final_result
		if season not in cdict["Seasons"]:
			cdict["Seasons"][season] = 0
		cdict["Seasons"][season] += final_result
		cdict[key]["Total"] += final_result
		if season not in cdict[key]["Seasons"]:
			cdict[key]["Seasons"][season] = 0
		cdict[key]["Seasons"][season] += final_result
		return cdict

	def get_score(self):
		return self.score_dict

	def insert_score(self):
		return

	def isValidTeam(self,teamId):
		'''
			Returns true if a given team is valid
			(a valid team is present in the db)
		'''
		return int(teamId) in self.teamsHePlayedData

	def isNationalTeam(self,teamId):
		'''
			Returns true if a team is a national team
		'''
		return self.teamsHePlayedData[int(teamId)]["NationalTeam"]

	def add_to_add_champ(self,champ,ctype,trigger="Player",team=""):
		'''
			Add a championship into the toAdd structure
			meaning the championship is missing in the database
		'''
		if champ not in self.toAdd["Championships"]:
			if trigger == "Player":
				self.toAdd["Championships"][champ] = {"name":champ,"type":ctype,"TriggerPlayer":[self.idPlayer],"TriggerTeam":[]}
			else:
				self.toAdd["Championships"][champ] = {"name":champ,"type":ctype,"TriggerPlayer":[],"TriggerTeam":[team]}
		else:
			if trigger == "Player" and self.idPlayer not in self.toAdd["Championships"][champ]["TriggerPlayer"]:
				self.toAdd["Championships"][champ]["TriggerPlayer"].append(self.idPlayer)
			if trigger != "Player" and team not in self.toAdd["Championships"][champ]["TriggerTeam"]:
				self.toAdd["Championships"][champ]["TriggerTeam"].append(team)


		return

	def find_champ_weight(self,champ,season=""):
		'''
			Returns the champion weight
		'''
		if champ not in self.champsCache:
			champ_weight = calculateUtils.find_champ_weight(champ,season=season,db=self.db)
			if champ_weight["champ_id"] not in calculateUtils.exception_champs_weights:
				self.champsCache[champ] = champ_weight
		else:
			champ_weight = self.champsCache[champ]
		return champ_weight

	def find_country_season_weight(self,country,season):
		'''
			Returns a country-season weight
		'''
		if country not in self.countryCache:
			self.countryCache[country] = {}
		if season not in self.countryCache[country]:
			country_weight = calculateUtils.find_country_season_weight(country,season,db=self.db)
			self.countryCache[country][season] = country_weight
		else:
			country_weight = self.countryCache[country][season]
		return country_weight

	def find_continent_season_weight(self,continent,season,country,team):
		'''
			Returns a continent-season weight
		'''
		if continent == None and country in ["Continental","National Continental"]:
			continent = calculateUtils.find_continent_from_team(team)
		if continent not in self.continentCache:
			self.continentCache[continent] = {}
		if country not in self.continentCache[continent]:
			self.continentCache[continent][country] = {}
		if season not in self.continentCache[continent][country]:
			continent_weight = calculateUtils.find_continent_season_weight(continent,season,country,db=self.db)
			self.continentCache[continent][country][season] = continent_weight
		else:
			continent_weight = self.continentCache[continent][country][season]
		return continent_weight

	def insert_score(self):
		insertScore.insert_score(self.playerData["_id"],self.playerData["transfermarkt_id"],self.resultantComponent)
		return

def main():

	player = 28003
	score = Calculate(player)

	return

if __name__ == "__main__":
	main()