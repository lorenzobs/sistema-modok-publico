'''
	Responsible for managing possible additions to database
	(for teams and championships that were mentioned in a player's data)
'''
import json
from pathlib import Path
import sys
import os
import time


sys.path.append(str(Path(__file__).absolute().parent.parent.parent))
from src.op_database import operations
from src.op_database import insertExtractions
from src.utils import utils

SCRIPT_PATH = str(Path(__file__).absolute().parent)
TO_ADD_PATH = SCRIPT_PATH + "/../../data/toAdd/"
TEAM_PATH = TO_ADD_PATH + "teams/"
CHAMP_PATH = TO_ADD_PATH + "championships/"
PLAYER_PATH = TO_ADD_PATH + "players/"
PLAYER_ADDITION_PATH = PLAYER_PATH + "additions/"

WAIT_FILE = "wait.json"
IGNORE_FILE = "ignore.json"
ADD_FILE = "add.json"


class toAddManager:

	def __init__(self):
		return 

	def load_jsons(self):
		'''
			Returns the json files responsible for managing
			the additions
		'''
		try:
			team_jsons = {}
			champ_jsons = {}

			team_jsons["wait"] = json.load(open(TEAM_PATH + WAIT_FILE,"r"))
			team_jsons["ignore"] = json.load(open(TEAM_PATH + IGNORE_FILE,"r"))
			team_jsons["add"] = json.load(open(TEAM_PATH + ADD_FILE,"r"))

			champ_jsons["wait"] = json.load(open(CHAMP_PATH + WAIT_FILE,"r"))
			champ_jsons["ignore"] = json.load(open(CHAMP_PATH + IGNORE_FILE,"r"))
			champ_jsons["add"] = json.load(open(CHAMP_PATH + ADD_FILE,"r"))
		except:
			raise Exception("[ERROR]: Fail to load addition files")
		return team_jsons,champ_jsons

	def write_json(self,path,content):
		with open(path,"w") as outfile:
			outfile.write(json.dumps(content,indent=1))


	def save_jsons(self,team_jsons,champ_jsons):
		'''
			Write jsons objects to each addition file
		'''
		fileTypes = {WAIT_FILE:"wait",IGNORE_FILE:"ignore",ADD_FILE:"add"}
		for f in [WAIT_FILE,IGNORE_FILE,ADD_FILE]:
			self.write_json(TEAM_PATH + f,team_jsons[fileTypes[f]])
			self.write_json(CHAMP_PATH + f,champ_jsons[fileTypes[f]])
		return

	def search_team(self,teamId,team_jsons):
		'''
			Search for a team in the addition files
		'''
		if teamId == "":
			teamId = -1
		find = None
		for fileType in ["wait","ignore","add"]:
			for t in team_jsons[fileType]:
				if int(t) == int(teamId):
					find = {"fileType":fileType,"Content":team_jsons[fileType][t]}
					return find
		return find

	def search_champ(self,champ,champ_jsons):
		'''
			Search for a championship in the addition files
		'''
		find = None
		for fileType in ["wait","ignore","add"]:
			for c in champ_jsons[fileType]:
				if c == champ:
					find = {"fileType":fileType,"Content":champ_jsons[fileType][c]}
					return find
		return find

	def join_additions(self):
		for file in os.scandir(PLAYER_ADDITION_PATH):
			if "_addition.json" not in file.name:
				continue
			try:
				content = json.load(open(file.path,"r"))
				self.join_addition(content)
			except:
				raise Exception("Error loading file " + file.path)
			os.remove(file.path)
		return

	def new_addition(self,idPlayer,newAddition):
		path = PLAYER_ADDITION_PATH + str(idPlayer) + "_addition.json"
		self.write_json(path,newAddition)
		return

	def join_addition(self,newAddition):
		'''
			Deals with possible new additions
		'''
		team_jsons,champ_jsons = self.load_jsons()
		new_team_jsons = {"wait":team_jsons["wait"],"ignore":team_jsons["ignore"],"add":team_jsons["add"]}
		new_champ_jsons = {"wait":champ_jsons["wait"],"ignore":champ_jsons["ignore"],"add":champ_jsons["add"]}

		# teams
		for t in newAddition["Teams"]:
			s = self.search_team(t,team_jsons)
			if s == None:
				new_team_jsons["wait"][t] = {"name":newAddition["Teams"][t]["name"],
											 "NationalTeam":newAddition["Teams"][t]["NationalTeam"],
											 "TriggerPlayer":[newAddition["Teams"][t]["TriggerPlayer"]]
											 }
			else:
				newTrigger = team_jsons[s["fileType"]][str(t)]["TriggerPlayer"]
				if newAddition["Teams"][t]["TriggerPlayer"] not in newTrigger:
					newTrigger.append(newAddition["Teams"][t]["TriggerPlayer"])
				new_team_jsons[s["fileType"]][t] = {"name":team_jsons[s["fileType"]][str(t)]["name"],"NationalTeam":team_jsons[s["fileType"]][str(t)]["NationalTeam"],"TriggerPlayer":newTrigger}

		# championships
		for c in newAddition["Championships"]:
			s = self.search_champ(c,champ_jsons)
			if s == None:
				new_champ_jsons["wait"][c] = {"name":newAddition["Championships"][c]["name"],
											  "type":newAddition["Championships"][c]["type"],
											  "TriggerPlayer":[newAddition["Championships"][c]["TriggerPlayer"]],
											  "TriggerTeam":newAddition["Championships"][c]["TriggerTeam"]}
			else:
				newTriggerPlayer = champ_jsons[s["fileType"]][c]["TriggerPlayer"]
				if newAddition["Championships"][c]["TriggerPlayer"] not in newTriggerPlayer:
					newTriggerPlayer.append(newAddition["Championships"][c]["TriggerPlayer"])
				newTriggerTeam = champ_jsons[s["fileType"]][c]["TriggerTeam"]
				for triggerTeam in newAddition["Championships"][c]["TriggerTeam"]:
					if triggerTeam not in newTriggerTeam:
						newTriggerTeam.append(triggerTeam)
				new_champ_jsons[s["fileType"]][c] = {"name":champ_jsons[s["fileType"]][c]["name"],"type":champ_jsons[s["fileType"]][c]["type"],"TriggerPlayer":newTriggerPlayer,"TriggerTeam":newTriggerTeam}

		self.save_jsons(new_team_jsons,new_champ_jsons)
		return

	def analyze(self,mode,fileType):
		'''
			Analyzes the addition files, managing
			if a team/champ should be added,ignored or
			waited
		'''
		self.join_additions()
		self.check_jsons()
		team_jsons,champ_jsons = self.load_jsons()
		if mode == "teams":
			mode = "team"
		if mode == "team":
			fileToAnalyze = team_jsons[fileType]
		else:
			fileToAnalyze = champ_jsons[fileType]

		if (len(fileToAnalyze) == 0):
			print("Nothing to analyze in " + fileType + " file")
			return
		print("Analyzing " + fileType + " file:")

		to_add = {"add":[],"wait":[],"ignore":[]}
		to_remove = {"add":[],"wait":[],"ignore":[]}

		try:
			for t in fileToAnalyze:
				name = fileToAnalyze[t]["name"]
				triggPlayer = fileToAnalyze[t]["TriggerPlayer"]
				print("#"*30)
				print("Name:",name)

				if mode == "team":
					print("TransfermarktId:",t)
					nat = fileToAnalyze[t]["NationalTeam"]
					print("National:",nat)
				else:
					ctype = fileToAnalyze[t]["type"]
					print("Type:",ctype)
					triggTeam = fileToAnalyze[t]["TriggerTeam"]
					print("TriggerTeam:",triggTeam)					
				print("TriggerPlayer:",triggPlayer)
				print("#"*30)
				ans = "None"
				while (ans not in ["A","W","I"]):
					ans = input("Add [A] | Wait [W] | Ignore [I]\n")
				if (ans == "A"): destType = "add"
				elif (ans == "W"): destType = "wait"
				else: destType = "ignore"

				if (destType == fileType): continue

				to_add[destType].append({t:fileToAnalyze[t]})
				to_remove[fileType].append({t:fileToAnalyze[t]})
		except KeyboardInterrupt:
			pass
		for t in to_remove:
			for r in to_remove[t]:
				Id = list(r.keys())[0]
				if mode == "team":
					del team_jsons[t][Id]
				else:
					del champ_jsons[t][Id]
		for t in to_add:
			for r in to_add[t]:
				Id = list(r.keys())[0]
				if mode == "team":
					team_jsons[t][Id] = r[Id]
				else:
					champ_jsons[t][Id] = r[Id]

		self.save_jsons(team_jsons,champ_jsons) 
		return


	def return_data_to_manage(self,mode):
		'''
			Return content of each add,wait and ignore jsons
		'''
		data_to_manage = {}

		self.join_additions()
		self.check_jsons()

		team_jsons,champ_jsons = self.load_jsons()

		if mode in ["team","teams"]:
			mode = "team"
			data_to_manage = team_jsons
		else:
			data_to_manage = champ_jsons
		data_to_manage = list(data_to_manage.items())
		for i in data_to_manage:
			data = []
			for obj_id in i[1]:
				obj = i[1][obj_id]
				obj["id"] = obj_id
				if mode == "team":
					data.append({"name":obj["name"],"id":obj["id"],"National Team":obj["NationalTeam"],"TriggerPlayer":obj["TriggerPlayer"]})
				else:
					del obj["name"]
					data.append(obj)
			if i[0] == "wait":
				wait = data
			elif i[0] == "add":
				add = data
			elif i[0] == "ignore":
				ignore = data

		sort_key = "id"
		if mode == "team":
			sort_key = "name"

		add = sorted(add, key=lambda d: d[sort_key])
		wait = sorted(wait, key=lambda d: d[sort_key])
		ignore = sorted(ignore, key=lambda d: d[sort_key])
		return [add,wait,ignore]

	def receive_data_to_manage(self,data_to_manage):
		'''
			Receives data with
			mode == team or championship
			and a dict with id ad key and wait/ignore/add
			as value
		'''
		self.join_additions()
		self.check_jsons()

		team_jsons,champ_jsons = self.load_jsons()
		if data_to_manage["mode"] in ["team","teams"]:
			target_data = team_jsons
		else:
			target_data = champ_jsons

		for obj_id in data_to_manage["data"]:
			action = data_to_manage["data"][obj_id]
			if obj_id in target_data[action]:
				continue
			else:
				data_to_change = {}	
				if obj_id in target_data["wait"]:
					data_to_change = target_data["wait"][obj_id]
					del target_data["wait"][obj_id]
				elif obj_id in target_data["add"]:
					data_to_change = target_data["add"][obj_id]
					del target_data["add"][obj_id]
				else:
					data_to_change = target_data["ignore"][obj_id]
					del target_data["ignore"][obj_id]
				
				target_data[action][obj_id] = data_to_change
		
		if data_to_manage["mode"] in ["teams","teams"]:
			self.save_jsons(target_data,champ_jsons)
		else:
			self.save_jsons(team_jsons,target_data)
		return


	def check_jsons(self,db=""):
		'''
			Check if each team/champ was not added at the db
		'''
		team_jsons,champ_jsons = self.load_jsons()
		new_team_jsons = {"wait":{},"ignore":{},"add":{}}
		new_champ_jsons = {"wait":{},"ignore":{},"add":{}}
		
		for fileType in ["wait","ignore","add"]:
			for t in team_jsons[fileType]: 
				query = {"filter":{"transfermarkt_id":int(t)}}
				response = operations.query_collection("TEAM",query,db=db)
				count = len(list(response.clone()))
				if count == 0:
					new_team_jsons[fileType][t] = team_jsons[fileType][t]
			for c in champ_jsons[fileType]:
				ctype = champ_jsons[fileType][c]["type"]
				if ctype == "Id":
					queries = [{"filter":{"_id":str(c).upper()}}]
				elif ctype == "Champion" or ctype == "champ or runnerup":
					queries = [utils.query_champion(c),
							   utils.query_runnerup(c)]
				count = 0
				for query in queries:
					response = operations.query_collection("CHAMPIONSHIP",query,db=db)
					count += len(list(response.clone()))
				if count == 0:
					new_champ_jsons[fileType][c] = champ_jsons[fileType][c]			

		self.save_jsons(new_team_jsons,new_champ_jsons)	

	def add_teams(self,debug=False):
		self.join_additions()
		self.check_jsons()
		team_jsons,_ = self.load_jsons()
		to_add = team_jsons["add"]

		team_list = list(to_add.keys())
		insertExtractions.insert_multiple_teams(team_list,debug=debug)
		self.check_jsons()
		return