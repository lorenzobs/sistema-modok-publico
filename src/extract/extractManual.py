'''
Responsible for extracting manual information from a certain player or club.
The source is stored in ../../data/manualData
'''

import pandas as pd
from pathlib import Path
import os,sys,json

sys.path.append(str(Path(__file__).absolute().parent.parent))
from utils import utils
from utils import extractUtils

manualDataPath = str((Path(__file__).absolute().parent.parent.parent)) + "/data/manualData/"
clubsManualDataPath = manualDataPath + "clubs"
playersManualDataPath = manualDataPath + "players"


class extractManual:

	def __init__(self,idManual,role="Player"):
		self.idManual = idManual
		self.role = role

		if role not in ["Player","Club"]:
			raise Exception("Invalid role: " + role)
		
		return

	def manualPlayer(self,oldData):

		def manual_player_achievements(oldData):
			'''
				Add or remove a player achievement	
			'''
			newData = {}
			path = playersManualDataPath + "/achievements/" + str(self.idManual) + ".csv"
			if os.path.exists(path) == False:
				return oldData
			df = pd.read_csv(path,encoding='utf8')
			try:
				toRemoveDf = df[df["action"] == "Remove"].reset_index()
				toAddDf = df[df["action"] == "Add"].reset_index()

				toModify = {}
				for i in range(0,len(toRemoveDf["season"])):
					season = utils.convert_season(toRemoveDf["season"][i])
					ach = toRemoveDf["achievement"][i].replace("","")
					club = int(toRemoveDf["clubId"][i])
					if season not in toModify:
						toModify[season] = {}
					if ach not in toModify[season]:
						toModify[season][ach] = {}
					if club not in toModify[season][ach]:
						toModify[season][ach][club] = {"Remove":False,"Add":False}
					toModify[season][ach][club]["Remove"] = True

				for i in range(0,len(toAddDf["season"])):
					season = utils.convert_season(toAddDf["season"][i])
					ach = toAddDf["achievement"][i].replace("","")
					club = int(toAddDf["clubId"][i])
					if season not in toModify:
						toModify[season] = {}
					if ach not in toModify[season]:
						toModify[season][ach] = {}
					if club not in toModify[season][ach]:
						toModify[season][ach][club] = {"Remove":False,"Add":False,"times":0}
					toModify[season][ach][club]["Add"] = True	
					toModify[season][ach][club]["times"] +=1
			except:
				raise Exception("Invalid format:" + path + "\n\t Check the README.md file inside " + playersManualDataPath + "/achievements/")

			toModify = cast_key_str_dict(toModify)
			oldData = cast_key_str_dict(oldData)

			for s in oldData:
				for ch in oldData[s]:
					for cl in oldData[s][ch]:
						season = s
						champId = ch
						club = cl["TeamId"]
						if season in toModify and champId in toModify[season] and club in toModify[season][champId]:
							if toModify[season][champId][club]["Remove"] == True:
								continue
						if s not in newData:
							newData[s] = {}
						if champId not in newData[s]:
							newData[s][champId] = []
						newData[s][champId].append({"TeamId":club})

			for s in toModify:
				for ch in toModify[s]:
					for cl in toModify[s][ch]:
						if toModify[s][ch][cl]["Add"] == True:
							times = toModify[s][ch][cl]["times"]
							if s not in newData:
								newData[s] = {}
							if ch not in newData[s]:
								newData[s][ch] = []
							# Forbid duplicates
							# times = 1
							for t in range(0,times):
								struct = {"TeamId":cl}
								newData[s][ch].append(struct)
			newData = dict(sorted(newData.items(), key=lambda x:int(x[0]), reverse=True))
			newData = cast_key_str_dict(newData)
			return newData

		def manual_player_performance(oldData):

			def line_per_match(oldData,path):
				'''
					Manual adition when each line of the csv contains a match
				'''
				df = pd.read_csv(path,encoding='utf8')
				toAdd = {}
				try:
					for i in range(0,len(df)):
						date = df["date"][i].replace("","").replace("?","1")
						champId = df["champId"][i].replace("","")
						clubId = int(df["clubId"][i])
						scoredGoals = int(max(0,df["scored goals"][i].astype('int64')))
						concededGoals = int(max(0,df["conceded goals"][i].astype('int64')))
						goals = int(max(0,df["goals"][i].astype('int64')))
						assists = int(max(0,df["assists"][i].astype('int64')))
						savedPenalties = int(max(0,df["saved penalties"][i].astype('int64')))

						season = utils.date_to_season(date,typeOfSeason=extractUtils.get_typeOfSeason(champId))
						if season not in toAdd:
							toAdd[season] = {}
						if champId not in toAdd[season]:
							toAdd[season][champId] = {}
						if clubId not in toAdd[season][champId]:
							toAdd[season][champId][clubId] = {"Matches":0,"Goals":0,"Assists":0,"Hat-tricks":0,"GWSG":0,"SavedPenalties":0,"PPG":0}

						if extractUtils.positions[self.position] in ["Defender","Goalkeeper"]:
							if concededGoals == 0:
								toAdd[season][champId][clubId]["GWSG"] += 1

						toAdd[season][champId][clubId]["Matches"] += 1
						toAdd[season][champId][clubId]["Goals"] += goals
						toAdd[season][champId][clubId]["Assists"] += assists
						if goals >= 3:
							toAdd[season][champId][clubId]["Hat-tricks"] += 1
						toAdd[season][champId][clubId]["SavedPenalties"] += savedPenalties
						if scoredGoals > concededGoals:
							toAdd[season][champId][clubId]["PPG"] =(toAdd[season][champId][clubId]["PPG"]*(toAdd[season][champId][clubId]["Matches"]-1)+3)/(toAdd[season][champId][clubId]["Matches"])
						elif scoredGoals == concededGoals:
							toAdd[season][champId][clubId]["PPG"] =(toAdd[season][champId][clubId]["PPG"]*(toAdd[season][champId][clubId]["Matches"]-1)+1)/(toAdd[season][champId][clubId]["Matches"])
						else:
							toAdd[season][champId][clubId]["PPG"] =(toAdd[season][champId][clubId]["PPG"]*(toAdd[season][champId][clubId]["Matches"]-1))/(toAdd[season][champId][clubId]["Matches"])
						
				except Exception as e:
					raise Exception("Invalid format:" + path + \
						"\n\t Check the README.md file inside " + playersManualDataPath + "/performance\n",e)
				
				toAdd = cast_key_str_dict(toAdd)
				oldData = cast_key_str_dict(oldData)
				for s in toAdd:
					for ch in toAdd[s]:
						for cl in toAdd[s][ch]:
							if s not in oldData:
								oldData[s] = {}
							if ch not in oldData[s]:
								oldData[s][ch] = {}
							if cl not in oldData[s][ch]:
								oldData[s][ch][cl] = {}
							oldData[s][ch][cl] = toAdd[s][ch][cl]
				newData = dict(sorted(oldData.items(), key=lambda x:int(x[0]), reverse=True))
				newData = cast_key_str_dict(newData)
				return newData

			def line_per_champ(oldData,path):
				'''
					Manual adition when each line of the csv contains a championship
				'''
				df = pd.read_csv(path,encoding='utf8')
				toAdd = {}
				try:
					for i in range(0,len(df)):
						champId = df["champId"][i].replace("","")
						season = utils.convert_season(df["season"][i])
						clubId = int(df["clubId"][i])
						matches = int(max(0,df["matches"][i].astype('int64')))
						goals = int(max(0,df["goals"][i].astype('int64')))
						assists = int(max(0,df["assists"][i].astype('int64')))
						hattricks = int(max(0,df["hat-tricks"][i].astype('int64')))
						cleanSheets = int(max(0,df["clean sheets"][i].astype('int64')))
						savedPenalties = int(max(0,df["saved penalties"][i].astype('int64')))
						ppg = float(max(0,df["PPG"][i].astype('float64')))

						if season not in toAdd:
							toAdd[season] = {}
						if champId not in toAdd[season]:
							toAdd[season][champId] = {}
						if clubId not in toAdd[season][champId]:
							toAdd[season][champId][clubId] = {"Matches":0,"Goals":0,"Assists":0,"Hat-tricks":0,"GWSG":0,"SavedPenalties":0,"PPG":0}

						toAdd[season][champId][clubId]["Matches"] = matches
						toAdd[season][champId][clubId]["Goals"] = goals
						toAdd[season][champId][clubId]["Assists"] = assists
						toAdd[season][champId][clubId]["Hat-tricks"] = hattricks
						toAdd[season][champId][clubId]["GWSG"] = cleanSheets
						toAdd[season][champId][clubId]["SavedPenalties"] = savedPenalties
						toAdd[season][champId][clubId]["PPG"] = ppg

				except:
					raise Exception("Invalid format:" + path + \
						"\n\t Check the README.md file inside " + playersManualDataPath + "/performance")
				
				toAdd = cast_key_str_dict(toAdd)
				oldData = cast_key_str_dict(oldData)
				
				for s in toAdd:
					for ch in toAdd[s]:
						for cl in toAdd[s][ch]:
							if s not in oldData:
								oldData[s] = {}
							if ch not in oldData[s]:
								oldData[s][ch] = {}
							if cl not in oldData[s][ch]:
								oldData[s][ch][cl] = {}
							oldData[s][ch][cl] = toAdd[s][ch][cl]
				newData = dict(sorted(oldData.items(), key=lambda x:int(x[0]), reverse=True))
				newData = cast_key_str_dict(newData)
				return newData

			pathChamps = playersManualDataPath + "/performance/" + str(self.idManual) + "_champs.csv"
			pathMatches = playersManualDataPath + "/performance/" + str(self.idManual) + "_matches.csv"

			existsChamps = os.path.exists(pathChamps)
			existsMatches = os.path.exists(pathMatches)
			if existsChamps == False and existsMatches == False:
				return oldData
			elif existsChamps == True and existsMatches == False:
				return line_per_champ(oldData,pathChamps)
			elif existsChamps == False and existsMatches == True:
				return line_per_match(oldData,pathMatches)
			else:
				final = line_per_champ(line_per_match(oldData,pathMatches),pathChamps)
				return final

		if self.data == "Achievements":
			return manual_player_achievements(oldData)

		return manual_player_performance(oldData)

	def manualClub(self,oldData):
		'''
			Receives the data extracted from the web and returns the
			data corrected by files in ../../data/manualData/clubs.
			It corrects the achievements of a club
		'''
		newData = {}
		currentClubPath = clubsManualDataPath + "/" + str(self.idManual) + ".csv"
		if os.path.exists(currentClubPath) == False:
			return oldData

		df = pd.read_csv(currentClubPath,encoding='utf8')
		try:
			toRemoveDf = df[df["action"] == "Remove"].reset_index()
			toAddDf = df[df["action"] == "Add"].reset_index()

			toRemoveDf = df[df["action"] == "Remove"].reset_index()
			toAddDf = df[df["action"] == "Add"].reset_index()
			toModify = {}
			for i in range(0,len(toRemoveDf["season"])):
				season = utils.convert_season(toRemoveDf["season"][i])
				ach = toRemoveDf["achievement"][i].replace("","")
				if season not in toModify:
					toModify[season] = {}
				if ach not in toModify[season]:
					toModify[season][ach] = {"Remove":False,"Add":False}
				toModify[season][ach]["Remove"] = True
			for i in range(0,len(toAddDf["season"])):
				season = utils.convert_season(toAddDf["season"][i])
				ach = toAddDf["achievement"][i].replace("","")
				if season not in toModify:
					toModify[season] = {}
				if ach not in toModify[season]:
					toModify[season][ach] = {"Remove":False,"Add":False}
				toModify[season][ach]["Add"] = True			
		except:
			raise Exception("Invalid format:" + path + "\n\t Check the README.md file inside " + clubsManualDataPath)

		toModify = cast_key_str_dict(toModify)
		oldData = cast_key_str_dict(oldData)
		for s in oldData:
			for c in oldData[s]:
				titleName = c["titleName"]
				season = s
				if season in toModify and titleName in toModify[season]:
					if toModify[season][titleName]["Remove"] == True:
						continue
				if s not in newData:
					newData[s] = []
				newData[s].append({"titleName":c["titleName"]})
		for s in toModify:
			for c in toModify[s]:
				if toModify[s][c]["Add"] == True:
					if s not in newData:
						newData[s] = []
					struct = {"titleName":c}
					# Forbid duplicates
					# if struct in newData[s]:
						# continue
					newData[s].append(struct)

		newData = dict(sorted(newData.items(), key=lambda x:int(x[0]), reverse=True))
		newData = cast_key_str_dict(newData)
		return newData

	def correctData(self,oldData,data="Achievements",position=""):
		'''
			The function receives the data to be corrected and call the function
			responsible for it depending on "data" and "role" variable
		'''
		if data not in ["Achievements","Performance"]:
			raise Exception("Invalid data: " + data)
		self.data = data
		self.position = position

		if self.role == "Player":
			return self.manualPlayer(oldData)
		else:
			if data != "Achievements":
				raise Exception("Invalid data for clubs:" + data)
			return self.manualClub(oldData)



def cast_key_str_dict(dic):
	'''
		Receives a dict and returns the
		same dict but with all keys as string
	'''
	cast_key_str = {}
	for key,value in dic.items():
		if isinstance(value,dict):
			cast_key_str[str(key)] = cast_key_str_dict(value)
		else:
			cast_key_str[str(key)] = value
	return cast_key_str