'''
	Responsible for gathering all information from a certain player.
	It uses the playerAPI module to receives each part of the information
'''

from . import extractManual
from .playerAPI import playerAPI
import json
import time

class extractPlayer:

	def __init__(self,idPlayer,debug=False):
		self.idPlayer = idPlayer
		self.debug = debug
		self.APIplayer = playerAPI(idPlayer,debug)
		self.nationalTeamId = ""
		self.position = ""
		self.extractionStruct = {"Name":"","TransfermarktId":self.idPlayer,"BirthDate":"","Nationality":"","Position":"",
								"Teams":{},"Achievements":{},"Performance":{},
								}
		return

	def get_full_extraction(self):
		'''
			Full extraction: players general information,
			transfer history, achievements and individual statistics
		'''

		manual = extractManual.extractManual(self.idPlayer,role="Player")

		# Basic info
		if self.extractionStruct["Name"] == "":
			self.get_basic_extraction()
		self.position = self.APIplayer.position

		# Team history
		teams = self.APIplayer.get_teams()
		self.extractionStruct["Teams"] = teams

		# National team id
		self.nationalTeamId = self.APIplayer.get_national_team()["TeamId"]

		# Achievements
		achievements = self.APIplayer.get_achievements()
		achievements = manual.correctData(achievements,data="Achievements")
		self.extractionStruct["Achievements"] = achievements

		# Individual performance
		performance = self.APIplayer.get_performance()
		performance = manual.correctData(performance,data="Performance",position=self.position)
		self.extractionStruct["Performance"] = performance

		return self.extractionStruct

	def get_basic_extraction(self):
		'''
			Basic extraction: players general information
			such as name, birth, position and nationality.
		'''
		gen_info = self.APIplayer.get_general_info()
		self.extractionStruct["Name"] = gen_info["Name"]
		self.extractionStruct["BirthDate"] = gen_info["BirthDate"]
		self.extractionStruct["Nationality"] = gen_info["Nationality"]
		self.extractionStruct["Position"] = gen_info["Position"]
		return self.extractionStruct