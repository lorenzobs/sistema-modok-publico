'''
	Responsible for gathering all information from a certain club.
	It uses the clubAPI module to receives each part of the information
'''
from pathlib import Path
import sys

sys.path.append(str(Path(__file__).absolute().parent.parent))
from src.extract import extractManual

from src.extract.clubAPI import clubAPI

class extractClub:
	def __init__(self,idClub,debug=False):
		self.idClub = idClub
		self.debug = debug
		self.APIclub = clubAPI(idClub,debug)
		self.extractionStruct = {"Name":"","TransfermarktId":self.idClub,"Country":"","Achievements":{}}
		return

	def get_full_extraction(self):
		'''
			Full extraction: general info + achievements
		'''
		manual = extractManual.extractManual(self.idClub,role="Club")

		if self.extractionStruct["Name"] == "":
			self.get_basic_extraction()

		achievements = self.APIclub.get_achievements()
		achievements = manual.correctData(achievements,data="Achievements")

		self.extractionStruct["Achievements"] = achievements
		return self.extractionStruct

	def get_basic_extraction(self):
		'''
			Basic extraction: clubs general information such as name and country.
		'''
		gen_info = self.APIclub.get_general_info()
		self.extractionStruct["Name"] = gen_info["Name"]
		self.extractionStruct["Country"] = gen_info["Country"]
		return self.extractionStruct

def main():
	clubs = [131,7535,27,199,26731]
	for c in clubs[0:1]:
		club = extractClub(c,debug=True)
		full = club.get_full_extraction()
		print(full)
		# basic = club.get_basic_extraction()
		# print(basic)

if __name__ == "__main__":
	main()