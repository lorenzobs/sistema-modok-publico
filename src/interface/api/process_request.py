from flask import Flask, request, render_template_string
from pathlib import Path

from multiprocessing import Process

import time
import asyncio

modok_template_file = str(Path(__file__).absolute().parent.parent) + "/templates/"

def process_request(operation_manager):
	operation = "Unknown"
	category = "Unknown"
	ids = ""
	message = "Invalid request"
	if "operation" in request.form and "category" in request.form and "input_type" in request.form and ("ids" in request.form or "file" in request.files):
		operation = request.form["operation"]
		category = request.form["category"]
		input_type = request.form["input_type"]
		if input_type == "file":
			if "file" not in request.files:
				return invalid_request()
			file = request.files["file"]
			ids = file.read().decode("utf-8")
		else:
			if "ids" not in request.form or request.form["ids"].strip() == "":
				return invalid_request()
			ids = request.form["ids"]
		message = "Request successfully completed"
	else:
		return invalid_request()


	template_string = open(modok_template_file + "api-loading.html","r").read()

	operation_info = {
		"operation":operation,
		"category":category,
		"input_type":input_type,
		"id_list":ids,
	}

	process = Process(target=operation_manager.start_working,args=[operation_info])

	yield render_template_string(template_string)

	process.start()

def invalid_request():
	template_string = open(modok_template_file + "api-error.html","r").read()
	return render_template_string(template_string)