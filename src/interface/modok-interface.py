import logging
import os
import sys
import math

from flask import Flask, request, render_template_string, send_from_directory,abort

from superset.initialization import SupersetAppInitializer
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from src.interface.api.process_request import process_request
from src.interface.classes.modok_manager import OperationManager
from src.interface.classes.modok_manager import AdditionManager
from src.utils import interfaceUtils

from datetime import datetime

logger = logging.getLogger(__name__)

def create_app() -> Flask:
	app = Flask("superset")
	try:
		# Allow user to override our config completely
		config_module = os.environ.get("SUPERSET_CONFIG", "superset.config")
		app.config.from_object(config_module)

		app.config["TEMPLATES_AUTO_RELOAD"] = True
		
		app_initializer = app.config.get("APP_INITIALIZER", SupersetAppInitializer)(app)

		app_initializer.init_app()

		modok_template_file = str(Path(__file__).absolute().parent) + "/templates/"

		operation_manager = OperationManager()

		@app.route('/favicon.ico')
		def favicon():
			return send_from_directory(os.path.join(str(Path(__file__).absolute().parent), 'static'),
				'favicon.ico', mimetype='image/vnd.microsoft.icon')
		
		@app.route('/style.css')
		def style():
			return send_from_directory(os.path.join(str(Path(__file__).absolute().parent), 'static'),
				'style.css')

		@app.route('/chuteira.png')
		def boots():
			return send_from_directory(os.path.join(str(Path(__file__).absolute().parent), 'static'),
				'chuteira.png')

		@app.route('/CotaneBeach.otf')
		def font():
			return send_from_directory(os.path.join(str(Path(__file__).absolute().parent), 'static'),
				'CotaneBeach.otf')
		
		@app.route('/Coolvetica.otf')
		def font_titles():
			return send_from_directory(os.path.join(str(Path(__file__).absolute().parent), 'static'),
				'Coolvetica.otf')


		@app.route('/modok/')
		def menu():
			template_string = open(modok_template_file + "modok.html","r").read()
			return render_template_string(template_string)

		@app.route('/modok/operations/')
		def operations():
			template_string = open(modok_template_file + "operations.html","r").read()
			return render_template_string(template_string)

		@app.route('/modok/addition_manager/')
		def addition_manager():
			template_string = open(modok_template_file + "addition-manager.html","r").read()
			return render_template_string(template_string)

		@app.route('/modok/addition_manager/teams',methods=["POST","GET"])
		def manage_teams():
			addition_manager = AdditionManager()
			if request.method == "POST":
				try:
					addition_manager.receive_data_to_manage(request.json)
					return "OK",200
				except:
					return abort(400)

			
			data_to_manage = addition_manager.return_data_to_manage("team")
			return data_to_manage

		@app.route('/modok/addition_manager/championships',methods=["POST","GET"])
		def manage_championships():
			addition_manager = AdditionManager()
			if request.method == "POST":
				try:
					addition_manager.receive_data_to_manage(request.json)
					return "OK",200
				except:
					return abort(400)
			data_to_manage = addition_manager.return_data_to_manage("championships")
			return data_to_manage

		@app.route('/modok/api/answer',methods = ['POST'])
		def modok_api():
			return process_request(operation_manager)

		@app.route('/modok/api/stop_working')
		def stop_working():
			operation_manager.stop_working()
			template_string = open(modok_template_file + "operations.html","r").read()
			return render_template_string(template_string)

		@app.route('/modok/api/work_progress')
		def work_progress():
			work_progress = operation_manager.get_work_progress()
			id_list = operation_manager.get_ids()
			percentage = math.floor(100 * len(work_progress) / max(1,len(id_list)))
			return str(percentage)

		@app.route('/modok/api/work_completed/<string:elapsed_time>')
		def work_completed(elapsed_time=0):
			template_string = open(modok_template_file + "api-answer.html","r").read()
			operation = operation_manager.get_operation()
			category = operation_manager.get_category()
			ids = operation_manager.get_ids()
			work_progress = operation_manager.get_work_progress()
			elapsed_time_string = interfaceUtils.format_seconds(elapsed_time)
			operation_manager.reset_work()
			return render_template_string(template_string,operation=operation,category=category,ids=ids,work_progress=work_progress,elapsed_time=elapsed_time_string)

		@app.route('/modok/api/work_error')
		def work_error():
			template_string = open(modok_template_file + "api-error.html","r").read()
			return render_template_string(template_string)

		return app

	# Make SupersetAppInitializer that bootstrap errors ALWAYS get logged
	except Exception as ex:
		logger.exception("Failed to create app")
		raise ex

class SupersetApp(Flask):
	pass