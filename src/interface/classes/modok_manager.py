import time
import sys
import multiprocessing
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent.parent.parent))
sys.path.append(str(Path(__file__).absolute().parent))
sys.path.append(str(Path(__file__).absolute().parent.parent))
sys.path.append(str(Path(__file__).absolute().parent.parent.parent))


import config

from src.interface.modok_cli import full_calculate_players
from src.interface.modok_cli import insert_teams
from src.interface.modok_cli import remove_teams
from src.interface.modok_cli import remove_players
from src.interface.modok_cli import mount_manage_list
from src.calculation import manageToAdd


class OperationManager():

	def __init__(self):
		self.multiprocess_manager = multiprocessing.Manager()
		self.working = self.multiprocess_manager.Value(bool,False)
		self.work_progress = self.multiprocess_manager.Value(str,"")
		self.operation = self.multiprocess_manager.Value(str,"")
		self.category = self.multiprocess_manager.Value(str,"")
		self.ids = self.multiprocess_manager.Value(str,"")
		self.input_type = self.multiprocess_manager.Value(str,"")
		return

	def start_working(self,operation_info):

		self.set_operation(operation_info["operation"])
		self.set_category(operation_info["category"])
		self.set_input_type(operation_info["input_type"])
		self.set_ids([])
		self.set_work_progress([])
		self.set_working_status(True)
		try:
			if self.get_operation() == "insert":
				if self.get_category() == "player":
					if self.get_input_type() == "file":
						id_list = mount_manage_list(None,operation_info["id_list"],"file-content","player")
					elif self.get_input_type() == "text":
						id_list = mount_manage_list(None,operation_info["id_list"],"csv","player")
					else:
						raise Exception("WRONG INPUT TYPE")
					self.set_ids(id_list)
					full_calculate_players(id_list,callbackProgress=self.append_to_progress)
				elif self.get_category() == "team":
					if self.get_input_type() == "file":
						id_list = mount_manage_list(None,operation_info["id_list"],"file-content","team")
					elif self.get_input_type() == "text":
						id_list = mount_manage_list(None,operation_info["id_list"],"csv","team")
					else:
						raise Exception("WRONG INPUT TYPE")
					self.set_ids(id_list)
					insert_teams(id_list,callbackProgress=self.append_to_progress)
				else:
					print("WRONG CATEGORY")
			elif self.get_operation() == "remove":
				if self.get_category() == "player":
					if self.get_input_type() == "file":
						id_list = mount_manage_list(None,operation_info["id_list"],"file-content","player")
					elif self.get_input_type() == "text":
						id_list = mount_manage_list(None,operation_info["id_list"],"csv","player")
					else:
						raise Exception("WRONG INPUT TYPE")
					self.set_ids(id_list)
					remove_players(id_list,callbackProgress=self.append_to_progress)
				elif self.get_category() == "team":
					if self.get_input_type() == "file":
						id_list = mount_manage_list(None,operation_info["id_list"],"file-content","team")
					elif self.get_input_type() == "text":
						id_list = mount_manage_list(None,operation_info["id_list"],"csv","team")
					else:
						raise Exception("WRONG INPUT TYPE")
					self.set_ids(id_list)
					remove_teams(id_list,callbackProgress=self.append_to_progress)
				else:
					self.set_category("WRONG CATEGORY")
					print("WRONG CATEGORY")
			else:
				self.set_category("WRONG CATEGORY")
				print("WRONG CATEGORY")

		except:
			pass

		self.set_working_status(False)

	def append_to_progress(self,value,status):
		current = self.get_work_progress()
		if current is None:
			current = []
		current.append([value,status])
		self.set_work_progress(current)

	def set_working_status(self,status):
		self.working.value = status

	def set_work_progress(self,work_progress):
		self.work_progress.value = work_progress

	def set_operation(self,operation):
		self.operation.value = operation
	def set_category(self,category):
		self.category.value = category
	def set_ids(self,ids):
		self.ids.value = ids

	def set_input_type(self,t):
		self.input_type.value = t

	def get_working_status(self):
		return self.working.value

	def get_work_progress(self):
		return self.work_progress.value

	def get_operation(self):
		return self.operation.value
	def get_category(self):
		return self.category.value
	def get_ids(self):
		return self.ids.value
	def get_input_type(self):
		return self.input_type.value

	def reset_work(self):
		self.set_working_status(False)
		self.set_work_progress([])		
		self.set_operation("")
		self.set_category("")
		self.set_ids("")
		self.set_input_type("")
		return

	def stop_working(self):
		self.set_working_status(False)


class AdditionManager(manageToAdd.toAddManager):
	pass
