import argparse,sys
from pathlib import Path
import sys

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from src.calculation import manageToAdd
from src.calculation import calculate
from src.calculation import insertAndCalculate
from src.op_database import insertExtractions
from src.op_database import operations
from src.graph_database.operations import update_node_player

def parse_args():
	'''
		Argument parsing
	'''
	arg_parser = argparse.ArgumentParser()
	
	arg_parser.add_argument("-d","--debug",action='store_true',required=False,help='Debug mode')
	arg_parser.add_argument("--type",action='store',choices=["csv","file"],help='Input player/team list type (only when ---player-list/--team-list is used)',required=False)

	group_manage_to_add = arg_parser.add_argument_group('Manage additions',"Uses manageToAdd module to manage possible additions to database")
	group_manage_to_add.add_argument("--manage-to-add",action='store',choices=['championships', 'teams'],required=False,help='Addition type')
	manage_add_mode = group_manage_to_add.add_mutually_exclusive_group()
	
	manage_add_mode.add_argument("--analyze",action='store',choices=['add','ignore', 'wait'],required=False,help='Analyze possible additions')
	manage_add_mode.add_argument("--add","--add-teams",action='store_true',required=False,help='Automatically add teams present in teams/add.json')

	group_player = arg_parser.add_argument_group('Player managing commands',)
	player = group_player.add_mutually_exclusive_group()
	player.add_argument("-p","--player",action='store',required=False,help='Player id')
	player.add_argument("-pl","--player-list",action='store',required=False,help='Player list')
	
	manage_player_mode = group_player.add_mutually_exclusive_group()

	manage_player_mode.add_argument("--calculate",action='store_true',required=False,help='Insert a player in the db and calculate his score')
	manage_player_mode.add_argument("--formula",action='store_true',required=False,help='Only apply the formula to a player already present in db')
	manage_player_mode.add_argument("--insert",action='store_true',required=False,help='Only insert a player information into the database')
	manage_player_mode.add_argument("--remove-player",action='store_true',required=False,help='Remove a player from the database')
	
	group_team = arg_parser.add_argument_group("Team managing commands",)
	team = group_team.add_mutually_exclusive_group()
	team.add_argument("-t","--team",action='store',required=False,help='Team id')
	team.add_argument("-tl","--team-list",action='store',required=False,help='Team list')

	manage_team_mode = group_team.add_mutually_exclusive_group()
	manage_team_mode.add_argument("-u","--update-teams",action='store_true',required=False,help='Update a team information in the database')
	manage_team_mode.add_argument("--remove-team",action='store_true',required=False,help='Remove a team from the database')

	args = arg_parser.parse_args()


	manage_to_add = False
	player_manage = False
	team_manage = False
	if args.manage_to_add is not None or args.analyze is not None or args.add:
		manage_to_add = True
	if args.player is not None or args.player_list is not None or args.calculate or args.formula or args.insert or args.remove_player:
		player_manage = True
	if args.team is not None or args.team_list is not None or args.update_teams or args.remove_team:
		team_manage = True

	if manage_to_add and player_manage:
		arg_parser.error("Addition management and player management are mutually exclusive")
	if manage_to_add and team_manage:
		arg_parser.error("Addition management and team management are mutually exclusive")
	if player_manage and team_manage:
		arg_parser.error("Player management and team management are mutually exclusive")

	if not manage_to_add and not player_manage and not team_manage:
		arg_parser.print_help() 
		sys.exit(0)
	if args.manage_to_add is not None and (args.analyze is None and args.add == False):
		arg_parser.error("--manage-to-add needs --analyze or --add command")

	if args.manage_to_add == "championships" and args.add:
		arg_parser.error("--add is valid only with teams")

	if (args.analyze is not None or  args.add) and args.manage_to_add is None:
		arg_parser.error("--analyze and --add need --manage-to-add")

	if args.player_list is not None and args.player_list not in ["*","all","ALL"] and args.type is None:
		arg_parser.error("--player-list needs --type command")

	if (args.player_list is None and args.player is None) and (args.calculate or args.formula or args.insert):
		arg_parser.error("--calculate, --formula and --insert need a --player or --player-list")

	if (args.player is not None or args.player_list is not None) and (not args.calculate and not args.formula and not args.insert and not args.remove_player):
		arg_parser.error("missing type of management for player")

	if player_manage and (args.type == 'csv' and "," not in args.player_list):
		arg_parser.error("wrong type for player list")

	if args.team_list is not None and args.team_list not in ["*","all","ALL"] and args.type is None:
		arg_parser.error("--team-list needs --type command")

	if (args.team_list is None and args.team is None) and args.update_teams:
		arg_parser.error("--update-teams need a --team or --team-list")
	if (args.team is not None or args.team_list is not None) and not args.update_teams and not args.remove_team:
		arg_parser.error("missing type of management for team")
	if team_manage and (args.type == 'csv' and "," not in args.team_list):
		arg_parser.error("wrong type for team list")

	return args


def analyze_manage_to_add(category,mode,debug=False):
	manager = manageToAdd.toAddManager()
	manager.analyze(category,mode)
	return

def add_teams_manage_to_add(debug=False):
	manager = manageToAdd.toAddManager()
	manager.add_teams(debug=debug)
	return

def full_calculate_players(player_list,debug=False,callbackProgress=None):
	insertAndCalculate.insert_and_calculate_multiple(player_list,debug=debug,callbackProgress=callbackProgress)
	return

def formula_players(player_list,debug=False):
	for p in player_list:
		try:
			calculate.Calculate(p,debug=debug)
			update_node_player(p,debug=debug)
		except KeyboardInterrupt:
			break
		except Exception as e:
			print("[ERROR]: Player",p,"formula failed (" + str(e) + ")")
			continue
	return

def insert_players(player_list,debug=False):
	insertExtractions.insert_multiple_players(player_list,debug=debug)
	return

def insert_teams(team_list,debug=False,callbackProgress=None):
	insertExtractions.insert_multiple_teams(team_list,debug=debug,callbackProgress=callbackProgress)
	return

def get_all_teams():
	team_list = []
	r = operations.dump_collection("TEAM")
	for i in r:
		team_list.append(str(i["transfermarkt_id"]))
	return team_list

def get_all_players():
	player_list = []
	r = operations.dump_collection("PLAYER")
	for i in r:
		player_list.append(str(i["transfermarkt_id"]))
	return player_list

def remove_teams(team_list,debug=False,callbackProgress=None):
	print("remove_teams",team_list)
	insertExtractions.remove_multiple_teams(team_list,debug=debug,callbackProgress=callbackProgress)
	return

def remove_players(player_list,debug=False,callbackProgress=None):
	print("remove_players:",player_list)
	insertExtractions.remove_multiple_players(player_list,debug=debug,callbackProgress=callbackProgress)
	return

def mount_manage_list(entity,entity_list,typ,entity_type="player"):
	'''
		Return a list with the players/teams to be managed
	'''
	if entity is not None:
		try:
			b = int(entity)
		except:
			raise Exception("Player/Team id is invalid")
		return [int(entity)]

	return_entity_list = []
	if entity_list in ["*","all"]:
		if entity_type == "player":
			return_entity_list = get_all_players()
		else:
			return_entity_list = get_all_teams()

	if typ == "csv":
		return_entity_list = entity_list.split(",")
	elif typ == "file" or typ == "file-content":
		if typ == "file":
			entity_list_file = open(entity_list,"r").read()
		else:
			entity_list_file = entity_list
		if "," in entity_list_file:
			entity_list_file = entity_list_file.replace("\n","").replace(" ","").strip()
			return_entity_list = entity_list_file.split(",")
		elif "\n" in entity_list_file:
			entity_list_file = entity_list_file.replace(" ","\n").strip()
			return_entity_list = entity_list_file.split("\n")
		elif " " in entity_list_file:
			entity_list_file = entity_list_file.replace("\n"," ").strip()
			return_entity_list = entity_list_file.split(" ")

	return_entity_list_parsed = []
	for p in return_entity_list:
		if p == "": continue
		try:
			b = int(p)
		except:
			raise Exception("Play/team list contains invalid ids")
		return_entity_list_parsed.append(p)
	return_entity_list_parsed = [eval(i) for i in return_entity_list_parsed]
	return return_entity_list_parsed


def main():
	args = parse_args()

	if args.manage_to_add is not None:
		if args.analyze is not None:
			analyze_manage_to_add(args.manage_to_add,args.analyze,debug=args.debug)
		else: # add teams
			add_teams_manage_to_add(debug=args.debug)

	if args.player is not None or args.player_list is not None:

		player_list = mount_manage_list(args.player,args.player_list,args.type,entity_type="player")

		if args.calculate:
			full_calculate_players(player_list,debug=args.debug)
		elif args.formula:
			formula_players(player_list,debug=args.debug)
		elif args.insert: # args.insert
			insert_players(player_list,debug=args.debug)
		elif args.remove_player:
			remove_players(player_list,debug=args.debug)
	if args.team is not None or args.team_list is not None:
		team_list = mount_manage_list(args.team,args.team_list,args.type,entity_type="team")
		if args.update_teams:
			insert_teams(team_list,debug=args.debug)
		elif args.remove_team:
			remove_teams(team_list,debug=args.debug)

	return






if __name__ == "__main__":
	main()