## Neo4j version

1:5.11.0

## Execute neo4j

```
sudo systemctl start neo4j.service
```
and then 
```
neo4j start
```

## Initialize database

Every player insertion updates the graph automatically, but if you want to create/update the graph all at once execute

```
python src/graph_database/graphConversion.py
```

## Execute metrics

```
python src/graph_database/getMetrics.py
```

## Important

Most of the metrics depends on the Graph Data Science Plugin from Neo4j. Install it from: https://neo4j.com/deployment-center/#gds-tab
And add the .jar to the directory `plugins/`