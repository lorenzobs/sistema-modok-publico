from neo4j import GraphDatabase
import config
import os
def connect_database(uri=os.getenv('neo4j_uri'),user=os.getenv('neo4j_user'),password=os.getenv('neo4j_password')):
	'''
		Connects with neo4j database
	'''
	exception = Exception("[GRAPH DB ERROR]: Error connecting to database")
	try:
		db = GraphDatabase.driver(uri, auth=(user, password))
	except:
		raise exception
	return db