from pathlib import Path
import sys

from datetime import datetime

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from src.graph_database.connect import connect_database
from src.op_database import operations as operations_mongodb

undirected_graph_name = 'undirectedGraph'


def query_db(session,query,parameters=None):
	'''
		Execute a custom query at the neo4j database
	'''
	response = list(session.run(query, parameters))
	return response

def create_indexes(session):
	'''
		Create indexes for the database
	'''
	index_transfermarkt_id = """
		CREATE INDEX node_player_transfermark_id IF NOT EXISTS
		FOR (n:Player)
		ON
		(n.transfermarkt_id)
	"""
	index_name = """
		CREATE INDEX node_player_name IF NOT EXISTS
		FOR (n:Player)
		ON
		(n.name)
	"""
	query_db(session,index_transfermarkt_id)
	query_db(session,index_name)

def player_node_exists(transfermarkt_id,session):
	'''
		Returns true if a node exists, given
		the player transfermark_id
	'''
	query_result =  query_db(session,"""
		MATCH (p:Player {transfermarkt_id: """ + str(transfermarkt_id) + """})
		WITH COUNT(p) > 0  as node_exists
		RETURN node_exists
	""")
	node_exists = dict(query_result[0])["node_exists"]
	return node_exists

def players_edge_exists(p1,p2,session):
	'''
		Returns true if a edge between two players exists
	'''
	edge_exists = False

	query = """
		RETURN EXISTS( (:Player {transfermarkt_id:""" + str(p1) + """ })
		-[:PLAYED_WITH]-
		(:Player {transfermarkt_id:""" + str(p2)+ """ }) ) as edge_exists
	"""
	edge_exists = query_db(session,query)
	edge_exists = dict(edge_exists[0])["edge_exists"]
	return edge_exists

def create_player(name,transfermarkt_id,score,session):
	'''
		Create a node for some player
	'''
	date_now = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
	query = "CREATE (p:Player {" + "name: \"{}\",transfermarkt_id: {},score: {}".format(name,transfermarkt_id,score) + ',last_update: datetime("{}")'.format(date_now) + "})"
	query_db(session,query)

def update_player(transfermarkt_id,name,score,session):
	'''
		Update the node of some player
	'''
	date_now = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
	query = " MATCH (p:Player {" + "transfermarkt_id: {}".format(transfermarkt_id) + "})" +  'SET p.score = {} , p.name = "{}" , p.last_update = datetime("{}") RETURN p'.format(score,name,date_now)
	query_db(session,query)

def create_edge(p1,p2,info,session):
	'''
		Create and edge for two players
	'''
	query = """
		MATCH
		  (a:Player),
		  (b:Player)
		WHERE a.transfermarkt_id = """ + str(p1["transfermarkt_id"]) + """ AND b.transfermarkt_id = """ + str(p2["transfermarkt_id"]) \
		+ """ CREATE (a)-[rab:PLAYED_WITH {
			seasons: """ + str(info["seasons"]) + """,
			teams:""" + str(info["teams"]) + """,
			cost:""" + str(-1*len(info["teams"]) * len(info["seasons"])) + """,
			nationalTeam:""" + str(info["nationalTeam"]) + """}]->(b)""" \
		+ """ RETURN type(rab)"""
	query_db(session,query)

def update_edge(p1,p2,info,session):
	'''
		Update an edge from two players
	'''
	query = """
		MATCH (a:Player {transfermarkt_id:""" + str(p1["transfermarkt_id"]) + """})-[r:PLAYED_WITH]-(b:Player {transfermark_id:""" + str(p2["transfermarkt_id"]) +"""})
		SET r.seasons = """ + str(info["seasons"]) + """,
		r.teams = """ + str(info["teams"]) + """,
		r.cost = """ + str(-1*len(info["teams"]) * len(info["seasons"])) + """,
		r.nationalTeam = """ + str(info["nationalTeam"])
	query_db(session,query)

def reset_player_edges(transfermarkt_id,session):
	'''
		Delete the edges of some player
	'''
	query = """
		MATCH (:Player { """ + "transfermarkt_id: {}".format(transfermarkt_id) + """ })
		-[r:PLAYED_WITH]-
		(:Player {""" + "transfermarkt_id: {}".format(transfermarkt_id) + """}) 
		DELETE r
	"""
	query_db(session,query)

def create_player_edges(pairs,session,transfermarkt_id=""):
	'''
		Given a dict of player pairs
		create/update the edges of each pair
	'''
	for pair in pairs:
		p1 = pairs[pair]["p1"]
		p2 = pairs[pair]["p2"]

		if int(p1["transfermarkt_id"]) != int(transfermarkt_id):
			p1_exists = player_node_exists(p1["transfermarkt_id"],session)
			if not p1_exists:
				create_player("",p1["transfermarkt_id"],-1,session)
		if int(p2["transfermarkt_id"]) != int(transfermarkt_id):
			p2_exists = player_node_exists(p2["transfermarkt_id"],session)
			if not p2_exists:
				create_player("",p2["transfermarkt_id"],-1,session)

		info = pairs[pair]["info"]

		edge_exists = players_edge_exists(p1["transfermarkt_id"],p2["transfermarkt_id"],session)

		if not edge_exists:
			create_edge(p1,p2,info,session)
		else:
			update_edge(p1,p2,info,session)

def determine_common_appearances(transfermarkt_id,performance):
	'''
		Determine all players that played
		together with a target player in a certain
		season-championship-team
	'''
	player_appearances = {}
	for season in performance:
		if season not in player_appearances:
			player_appearances[season] = {}
		for champ in performance[season]:
			if champ not in player_appearances[season]:
				player_appearances[season][champ] = {}
			for team in performance[season][champ]:
				if team not in player_appearances[season][champ]:
					player_appearances[season][champ][team] = []

	r = operations_mongodb.dump_collection("PLAYER")
	for player in r:
		if int(player["transfermarkt_id"]) == int(transfermarkt_id):
			continue
		p2_performance = player["Performance"]
		for season in p2_performance:
			if season not in player_appearances:
				continue
			for champ in p2_performance[season]:
				if champ not in player_appearances[season]:
					continue
				for team in p2_performance[season][champ]:
					if team not in player_appearances[season][champ]:
						continue
					player_appearances[season][champ][team].append(int(player["transfermarkt_id"]))
	return player_appearances

def get_national_teams():
	'''
		Return all national teams in the database
	'''
	national_teams = []
	r = operations_mongodb.dump_collection("COUNTRY")
	for country in r:
		transfermarkt_team_id = country["transfermarkt_team_id"]
		national_teams.append(transfermarkt_team_id)
	return national_teams

def get_valid_champs():
	'''
		Return all championships ids in the database
	'''
	valid_champs = []
	r = operations_mongodb.dump_collection("CHAMPIONSHIP")
	for champ in r:
		champ_id = champ["_id"]
		valid_champs.append(champ_id)
	return valid_champs

def determine_pairs(transfermarkt_id,session,performance=""):
	'''
		Determine all pairs witch should be added to the 
		graph as edges
	'''
	transfermarkt_id = int(transfermarkt_id)
	if performance == "":
		performance = operations_mongodb.get_document_by_att("PLAYER",transfermarkt_id,att="transfermarkt_id")["Performance"]
	pairs = {}
	player_appearances = determine_common_appearances(transfermarkt_id,performance)
	national_teams = get_national_teams()
	valid_champs = get_valid_champs()
	for season in player_appearances:
		for champ in player_appearances[season]:
			champ_valid = operations_mongodb.query_collection("CHAMPIONSHIP",{"filter":{"_id":str(champ).upper()}})
			if str(champ).upper() not in valid_champs:
				continue
			for team in player_appearances[season][champ]:
				if int(team) in national_teams:
					continue
				for player in player_appearances[season][champ][team]:
					p1 = min(transfermarkt_id,player)
					p2 = max(transfermarkt_id,player)
					key_str = str(p1) + str(p2)
					if key_str not in pairs:
						pairs[key_str] = {
							"p1":{"transfermarkt_id":p1},
							"p2":{"transfermarkt_id":p2},
							"info":{
								"seasons":[season],
								"teams":[team],
								"nationalTeam":False
							}
						}
					else:
						if season not in pairs[key_str]["info"]["seasons"]:
							pairs[key_str]["info"]["seasons"].append(season)
						if team not in pairs[key_str]["info"]["teams"]:
							pairs[key_str]["info"]["teams"].append(team)
	return pairs

def update_node_player(transfermarkt_id,debug=False):
	'''
		Main function for creating/updating a player's node
	'''
	try:
		db = connect_database()
		session = db.session()

		create_indexes(session)

		player_document = operations_mongodb.get_document_by_att("PLAYER",transfermarkt_id,att="transfermarkt_id")

		player_score = operations_mongodb.get_document_by_att("SCORE",transfermarkt_id,att="transfermarkt_id")
		score = player_score["Total"]

		name = player_document["name"]
		node_exists = player_node_exists(transfermarkt_id,session)

		if not node_exists:
			create_player(name,transfermarkt_id,score,session)
		else:
			update_player(transfermarkt_id,name,score,session)

		session.close()
		db.close()
		print("Player",transfermarkt_id,"node was successfully updated")
	except Exception as e:
		print("[ERROR]: Player",transfermarkt_id,"node update FAILED (" + str(e) + ")")
	return

def update_edges_player(transfermarkt_id):
	'''
		Main function for creating/updating a players' edges
	'''
	try:
		db = connect_database()
		session = db.session()

		create_indexes(session)

		player_document = operations_mongodb.get_document_by_att("PLAYER",transfermarkt_id,att="transfermarkt_id")
		name = player_document["name"]
		performance = player_document["Performance"]
		node_exists = player_node_exists(transfermarkt_id,session)

		if not node_exists:
			create_player(name,transfermarkt_id,-1,session)

		reset_player_edges(transfermarkt_id,session)
		pairs = determine_pairs(transfermarkt_id,session,performance=performance)
		create_player_edges(pairs,session,transfermarkt_id=transfermarkt_id)

		session.close()
		db.close()
		print("Player",transfermarkt_id,"edges were successfully updated")		
	except Exception as e:
		print("[ERROR]: Player",transfermarkt_id,"edges update FAILED (" + str(e) + ")")
	return

def remove_undirected_gds_graph(session):
	'''
		Drop a gds undirected graph
	'''
	query = """CALL gds.graph.drop('""" + undirected_graph_name + """', false) YIELD graphName;"""
	query_db(session,query)
	return

def create_undirected_gds_graph(session):
	'''
		Create a gds undirected graph
	'''
	query = """
		
		CALL gds.graph.project(
			'""" + undirected_graph_name + """',
			'Player',
			{
				PLAYED_WITH:{
					orientation: 'UNDIRECTED'

				}
			},
			{
				relationshipProperties: 'cost'
			}
		)

	"""
	query_db(session,query)
	return
