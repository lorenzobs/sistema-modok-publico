from pathlib import Path
import sys
import math
sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from src.op_database import operations as operations_mongodb

from src.graph_database.connect import connect_database as connect_database_neo4j
from src.graph_database import operations as operations_neo4j


from tqdm import tqdm

def reset_database(session):
    operations_neo4j.query_db(session,'MATCH (a) -[r] -> () delete a, r')
    operations_neo4j.query_db(session,'MATCH (a) delete a')


def create_nodes_players(session,players):
    for player in tqdm(players,desc="Loading…"):
        
        transfermarkt_id = player["transfermarkt_id"]
        name = player["name"]
        score = player["score"]

        node_exists = operations_neo4j.player_node_exists(transfermarkt_id,session)

        if not node_exists:
            operations_neo4j.create_player(name,transfermarkt_id,score,session)
        else:
            operations_neo4j.update_player(transfermarkt_id,name,score,session)

def create_relation_players(session,pairs):
    for pair in tqdm(pairs,desc="Loading…"):
        p1 = pair["p1"]
        p2 = pair["p2"]
        info = pair["info"]

        edge_exists = operations_neo4j.players_edge_exists(p1["transfermarkt_id"],p2["transfermarkt_id"],session)

        if not edge_exists:
            operations_neo4j.create_edge(p1,p2,info,session)
        else:
            operations_neo4j.update_edge(p1,p2,info,session)

def get_players():
    player_list = []
    r = operations_mongodb.dump_collection("PLAYER")
    for player in r:
        document_score = operations_mongodb.get_document_by_att("SCORE",int(player["transfermarkt_id"]),att="transfermarkt_id")
        score_total = document_score["Total"]
        dic = {"transfermarkt_id":player["transfermarkt_id"],"name":player["name"],"score":math.floor(score_total)}
        player_list.append(dic)
    return player_list

def add_ids_pairs(new_ids,pair_list,pair_existance,season,teamId,nationalTeam,seasons_pairs,teams_pairs):
    pairs = [(a, b) for idx, a in enumerate(new_ids) for b in new_ids[idx + 1:]]

    if len(pairs) == 0:
        return pair_list,pair_existance,seasons_pairs,teams_pairs
    for pair in pairs:
        p1 = min(pair[0],pair[1])
        p2 = max(pair[0],pair[1])
        key_str = str(p1) + str(p2)

        if key_str not in seasons_pairs:
            seasons_pairs[key_str] = []
        if key_str not in teams_pairs:
            teams_pairs[key_str] = []

        if int(season) not in seasons_pairs[key_str]:
            seasons_pairs[key_str].append(int(season))
        if int(teamId) not in teams_pairs[key_str]:
            teams_pairs[key_str].append(int(teamId))
        
        if key_str in pair_existance:
            continue
        pair_existance.append(key_str)
        pair_list.append({"id":key_str,"p1":{"transfermarkt_id":p1},"p2":{"transfermarkt_id":p2},"info":{"seasons":[],"teams":[],"nationalTeam":nationalTeam}})    

    return pair_list,pair_existance,seasons_pairs,teams_pairs

def get_pairs():
    r = operations_mongodb.dump_collection("PLAYER")

    has_played_dict = {}
    national_teams = operations_neo4j.get_national_teams()
    valid_champs = operations_neo4j.get_valid_champs()
    for player in r:
        for season in player["Performance"]:
            if season not in has_played_dict:
                has_played_dict[season] = {}
            for champId in player["Performance"][season]:
                if champId not in valid_champs:
                    continue
                for teamId in player["Performance"][season][champId]:
                    if int(teamId) in national_teams:
                        continue
                    if teamId not in has_played_dict[season]:
                        has_played_dict[season][teamId] = {}
                    if champId not in has_played_dict[season][teamId]:
                        has_played_dict[season][teamId][champId] = []
                    has_played_dict[season][teamId][champId].append(player["transfermarkt_id"])


    pair_existance = []
    pair_list = []
    seasons_pairs = {}
    teams_pairs = {}
    for season in has_played_dict:
        for teamId in has_played_dict[season]:
            for champId in has_played_dict[season][teamId]:
                list_ids = has_played_dict[season][teamId][champId]
                nationalTeam = False
                pair_list,pair_existance,seasons_pairs,teams_pairs = add_ids_pairs(list_ids,pair_list,pair_existance,season,teamId,nationalTeam,seasons_pairs,teams_pairs)

    for i in range(0,len(pair_list)):
        seasons_pairs[pair_list[i]["id"]].sort()
        teams_pairs[pair_list[i]["id"]].sort()
        pair_list[i]["info"]["seasons"] = seasons_pairs[pair_list[i]["id"]]
        pair_list[i]["info"]["teams"] = teams_pairs[pair_list[i]["id"]]
    return pair_list

def convert_mongo_neo4j():
    db = connect_database_neo4j()
    session = db.session()

    print("Reseting database...")
    reset_database(session)


    print("Getting players...")
    players = get_players()
    print("Getting pairs...")
    pairs = get_pairs()

    print("Creating player nodes...")
    create_nodes_players(session,players)

    print("Creating player relationships...")
    create_relation_players(session,pairs)

    db.close()
    return

if __name__ == "__main__":
    convert_mongo_neo4j()