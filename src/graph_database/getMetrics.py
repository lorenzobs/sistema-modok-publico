from pathlib import Path
import sys
import math
import json
import os
import time
sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from src.graph_database.connect import connect_database as connect_database_neo4j
from src.graph_database import operations as operations_neo4j
from src.op_database import operations as operations_mongodb

from tqdm import tqdm


undirected_graph_name = operations_neo4j.undirected_graph_name

def get_query_shortest_path(p1,p2):

	queryShortestPath = """

		MATCH (source:Player {transfermarkt_id: """ + str(p1) + """}), (target:Player {transfermarkt_id:""" +  str(p2) +  """ })
		CALL gds.shortestPath.dijkstra.stream('""" + undirected_graph_name + """', {
		    sourceNode: source,
		    targetNode: target
		})
		YIELD index, sourceNode, targetNode, totalCost, nodeIds, path
		RETURN
		    index,
		    gds.util.asNode(sourceNode).name AS sourceNodeName,
		    gds.util.asNode(targetNode).name AS targetNodeName,
		    [nodeId IN nodeIds | gds.util.asNode(nodeId).name] AS nodeNames
		ORDER BY index
	"""
	return queryShortestPath

def get_query_shortest_path_cost(p1,p2):

	queryShortestPathCost = """

		MATCH (source:Player {transfermarkt_id: """ + str(p1) + """}), (target:Player {transfermarkt_id:""" +  str(p2) +  """ })
		CALL gds.shortestPath.dijkstra.stream('""" + undirected_graph_name + """', {
		    sourceNode: source,
		    targetNode: target,
		    relationshipWeightProperty: 'cost'
		})
		YIELD index, sourceNode, targetNode, totalCost, nodeIds, costs, path
		RETURN
		    index,
		    gds.util.asNode(sourceNode).name AS sourceNodeName,
		    gds.util.asNode(targetNode).name AS targetNodeName,
		    totalCost,
		    [nodeId IN nodeIds | gds.util.asNode(nodeId).name] AS nodeNames,
		    costs,
		    nodes(path) as path
		ORDER BY index
	"""
	return queryShortestPathCost

def get_query_k_colors():
	queryColors = """

		CALL gds.beta.k1coloring.stream('""" + undirected_graph_name + """')
		YIELD nodeId, color
		RETURN gds.util.asNode(nodeId).name AS name, color
		ORDER BY name

	"""
	return queryColors

def get_query_strongly_connected():
	queryStrongly = """

		CALL gds.alpha.scc.stream('""" + undirected_graph_name + """')
		YIELD nodeId, componentId
		RETURN gds.util.asNode(nodeId).name AS Name, componentId AS Component
		ORDER BY Component DESC

	"""
	return queryStrongly

def get_query_weakly_connected():
	queryWeakly = """

		CALL gds.wcc.stream('""" + undirected_graph_name + """')
		YIELD nodeId, componentId
		RETURN gds.util.asNode(nodeId).name AS Name, componentId AS Component
		ORDER BY Component DESC

	"""
	return queryWeakly

def get_query_triangle_count():
	queryTriangle = """

		CALL gds.triangleCount.stream('""" + undirected_graph_name + """')
		YIELD nodeId, triangleCount
		RETURN gds.util.asNode(nodeId).name AS name, triangleCount
		ORDER BY triangleCount DESC

	"""
	return queryTriangle

def get_query_degree_centrality():
	queryDegree = """
		CALL gds.degree.stream('""" + undirected_graph_name + """')
		YIELD nodeId, score
		RETURN gds.util.asNode(nodeId).name AS name, score AS followers
		ORDER BY followers DESC, name DESC
	"""
	return queryDegree

def get_query_influence(seedSetSize):
	queryDegree = """
		CALL gds.beta.influenceMaximization.celf.stream('""" + undirected_graph_name + """',{seedSetSize:""" + str(seedSetSize) + """})
		YIELD nodeId, spread
		RETURN gds.util.asNode(nodeId).name AS name, spread
		ORDER BY spread DESC, name ASC
	"""
	return queryDegree

def get_query_all_players():

	queryAllPlayers = """

		MATCH(n:Player)
		RETURN n


	"""
	return queryAllPlayers

def get_query_player_score(p1):
	queryPlayerScore = """
		MATCH(n:Player {transfermarkt_id:""" + str(p1) + """})
		RETURN n.score as score
	"""
	return queryPlayerScore

def get_query_neighbors(p1):
	queryNeighbors = """
		MATCH path=((n:Player)<-[*1..1]->(b:Player{transfermarkt_id:""" + str(p1) + """})) return n
	"""
	return queryNeighbors

def all_players(session):
	players_ids = []
	response = operations_neo4j.query_db(session,get_query_all_players())
	for player in response:
		players_ids.append(int(player["n"].get("transfermarkt_id")))
	return players_ids

def get_query_all_pairs_shortest_path():
	query = """

			CALL gds.allShortestPaths.stream('""" + undirected_graph_name + """', {
			})
			YIELD sourceNodeId, targetNodeId, distance
			WITH sourceNodeId, targetNodeId, distance
			WHERE gds.util.isFinite(distance) = true
			WITH gds.util.asNode(sourceNodeId) AS source, gds.util.asNode(targetNodeId) AS target, distance WHERE source <> target

			RETURN source.transfermarkt_id AS source, target.transfermarkt_id AS target, distance
			ORDER BY distance DESC, source ASC, target ASC
			LIMIT 1000
	"""
	return query

def get_query_all_pairs_shortest_path_cost():
	query = """

			CALL gds.allShortestPaths.stream('""" + undirected_graph_name + """', {
    			relationshipWeightProperty: 'cost'
  			})
			YIELD sourceNodeId, targetNodeId, distance
			WITH sourceNodeId, targetNodeId, distance
			WHERE gds.util.isFinite(distance) = true
			WITH gds.util.asNode(sourceNodeId) AS source, gds.util.asNode(targetNodeId) AS target, distance WHERE source <> target

			RETURN source.transfermarkt_id AS source, target.transfermarkt_id AS target, distance
			ORDER BY distance DESC, source ASC, target ASC
			LIMIT 1000
	"""
	return query

def metric_shortest_paths(session):
	print("Running shortest paths...")
	
	response = operations_neo4j.query_db(session,get_query_all_pairs_shortest_path())

	maximum_dist = dict(response[0])["distance"]
	shortest_paths = {"num_of_paths":0,"result":{}}
	for record in tqdm(response,desc="Loading…"):
		answer = dict(record)
		source = answer["source"]
		target = answer["target"]
		dist = answer["distance"]

		if dist < maximum_dist:
			break

		combined_id = str(source) + str(target)

		path = dict(operations_neo4j.query_db(session,get_query_shortest_path_cost(source,target))[0])["nodeNames"]
		
		shortest_paths["result"][combined_id] = {}
		shortest_paths["result"][combined_id]["distance"] = dist
		shortest_paths["result"][combined_id]["path"] = path

	shortest_paths["num_of_paths"] = len(list(shortest_paths["result"].keys()))
	return shortest_paths

def metric_shortest_paths_cost(session):
	print("Running shortest paths cost...")

	response = operations_neo4j.query_db(session,get_query_all_pairs_shortest_path_cost())

	maximum_dist = dict(response[0])["distance"]
	shortest_paths_cost = {"num_of_paths":0,"result":{}}
	for record in tqdm(response,desc="Loading…"):
		answer = dict(record)
		source = answer["source"]
		target = answer["target"]
		dist = answer["distance"]

		if dist < maximum_dist:
			break

		combined_id = str(source) + str(target)

		answer_path_cost = dict(operations_neo4j.query_db(session,get_query_shortest_path(source,target))[0])
		
		path = answer_path_cost["nodeNames"]
		cost = answer_path_cost["totalCost"]
		cost_path = answer_path_cost["costs"]
		
		shortest_paths_cost["result"][combined_id] = {}
		shortest_paths_cost["result"][combined_id]["dist"] = dist
		shortest_paths_cost["result"][combined_id]["cost"] = cost
		shortest_paths_cost["result"][combined_id]["cost_path"] = cost_path
		shortest_paths_cost["result"][combined_id]["path"] = path

	shortest_paths_cost["num_of_paths"] = len(list(shortest_paths_cost["result"].keys()))

	return shortest_paths_cost

def metric_k_colors(session):
	print("Running k colors...")
	response = operations_neo4j.query_db(session,get_query_k_colors())
	node_count = 0
	colors = []
	node_color = {}
	for node in response:
		node_count += 1
		if node["color"] not in colors:
			colors.append(node["color"])
		node_color[node["name"]] = node["color"]	

	return_dict = {
		"node_count":node_count,
		"color_count":len(colors),
		"node_color": node_color
	}
	return return_dict

def metric_strongly_connected_components(session):
	print("Running scc...")
	response = operations_neo4j.query_db(session,get_query_strongly_connected())
	node_count = 0
	components = []
	node_component = {}
	for node in response:
		node_count += 1
		if node["Component"] not in components:
			components.append(node["Component"])
		node_component[node["Name"]] = node["Component"]	

	return_dict = {
		"node_count":node_count,
		"component_count":len(components),
		"node_component": node_component
	}
	return return_dict

def metric_weakly_connected_components(session):
	print("Running wcc...")
	response = operations_neo4j.query_db(session,get_query_weakly_connected())
	node_count = 0
	components = []
	node_component = {}
	for node in response:
		node_count += 1
		if node["Component"] not in components:
			components.append(node["Component"])
		node_component[node["Name"]] = node["Component"]	

	return_dict = {
		"node_count":node_count,
		"component_count":len(components),
		"node_component": node_component
	}
	return return_dict

def metric_triangle_count(session):
	print("Running triangle count...")
	response = operations_neo4j.query_db(session,get_query_triangle_count())
	node_count = 0
	node_triangles = {}
	for node in response:
		node_count += 1
		node_triangles[node["name"]] = node["triangleCount"]	

	sorted_node_triangles = dict(sorted(node_triangles.items(), key=lambda x:int(x[1]),reverse=True))

	return_dict = {
		"node_count":node_count,
		"node_triangles": sorted_node_triangles 
	}
	return return_dict	

def metric_degree_centrality(session):
	print("Running centrality...")
	response = operations_neo4j.query_db(session,get_query_degree_centrality())
	node_count = 0
	node_followers = {}
	for node in response:
		node_count += 1
		node_followers[node["name"]] = node["followers"]	

	sorted_node_followers = dict(sorted(node_followers.items(), key=lambda x:int(x[1]),reverse=True))

	return_dict = {
		"node_count":node_count,
		"node_followers": sorted_node_followers 
	}
	return return_dict

def metric_influence(session,seedSetSize=3):
	print("Running influence...")
	response = operations_neo4j.query_db(session,get_query_influence(seedSetSize))
	node_count = 0
	node_influence = {}
	for node in response:
		node_count += 1
		node_influence[node["name"]] = node["spread"]	

	return_dict = {
		"node_count":node_count,
		"node_influence": node_influence
	}
	return return_dict

def metric_score_neighbors(session):
	print("Running score neighbors...")
	players_ids = all_players(session)

	player_neighbor_score = {}
	for player_id in tqdm(players_ids,desc="Loading…"):
		response = operations_neo4j.query_db(session,get_query_player_score(player_id))
		document_score = operations_mongodb.get_document_by_att("SCORE",int(player_id),att="transfermarkt_id")
		player_score_individual = int(document_score["C1"])
		player_score = response[0]["score"]
		neighbor_list = operations_neo4j.query_db(session,get_query_neighbors(player_id))
		sum_score_neighbors = 0
		sum_score_neighbors_individual = 0
		for neighbor_struct in neighbor_list:
			neighbor = dict(neighbor_struct["n"])
			response = operations_neo4j.query_db(session,get_query_player_score(neighbor["transfermarkt_id"]))
			neighbor_score = response[0]["score"]
			document_score = operations_mongodb.get_document_by_att("SCORE",int(neighbor["transfermarkt_id"]),att="transfermarkt_id")
			neighbor_score_individual = int(document_score["C1"])
			sum_score_neighbors += int(neighbor_score)
			sum_score_neighbors_individual += int(neighbor_score_individual)
		try:
			mean_neighbors_score = int(sum_score_neighbors / len(neighbor_list))
		except:
			mean_neighbors_score = 0
		try: 
			mean_neighbors_score_individual = int(sum_score_neighbors_individual / len(neighbor_list))
		except:
			mean_neighbors_score_individual = 0
		player_neighbor_score[player_id] = {"player_score":player_score,"player_score_individual":player_score_individual,"neighbor_score":mean_neighbors_score,"neighbor_score_individual":mean_neighbors_score_individual,"neighbor_count":len(neighbor_list)}

	return_dict = {
		"node_count":len(players_ids),
		"player_neighbor_score": player_neighbor_score
	}
	return return_dict



def main():
	start = time.time()
	db = connect_database_neo4j()
	session = db.session()

	report_dir = str(str(Path(__file__).absolute().parent)) + "/reports/"
	report_file_name = report_dir + "report-" + time.strftime("%Y%m%d-%H%M%S")

	operations_neo4j.remove_undirected_gds_graph(session)
	operations_neo4j.create_undirected_gds_graph(session)


	report = {"results":{}}
	
	try:
		metric_colors = metric_k_colors(session)
	except:
		metric_colors = "Error running metric_k_colors"
	report["results"]["metric_colors"] = metric_colors 

	try:
		metric_scc = metric_strongly_connected_components(session)
	except:
		metric_scc = "Error running metric_strongly_connected_components"
	report["results"]["metric_scc"] = metric_scc 
	try:
		metric_wcc = metric_weakly_connected_components(session)
	except:
		metric_wcc = "Error running metric_weakly_connected_components"
	report["results"]["metric_wcc"] = metric_wcc
	
	try:
		metric_triangle = metric_triangle_count(session)
	except:
		metric_triangle = "Error running metric_triangle_count"
	report["results"]["metric_triangle"] = metric_triangle

	try:
		metric_centrality = metric_degree_centrality(session)
	except:
		metric_centrality = "Error running metric_degree_centrality"
	report["results"]["metric_centrality"] = metric_centrality
	
	try:
		metric_influence_max = metric_influence(session,seedSetSize=10)
	except:
		metric_influence_max = "Error running metric_influence"
	report["results"]["metric_influence_max"] = metric_influence_max
	
	try:
		score_neighbors = metric_score_neighbors(session)
	except:
		score_neighbors = "Error running metric_score_neighbors"
	report["results"]["score_neighbors"] = score_neighbors

	try:
		metric_shortest = metric_shortest_paths(session)
	except:
		metric_shortest = "Error running metric_shortest_paths"
	report["results"]["metric_shortest"] = metric_shortest

	try:
		metric_shortest_cost = metric_shortest_paths_cost(session)
	except:
		metric_shortest_cost = "Error running metric_shortes_paths_cost"	
	report["results"]["metric_shortest_cost"] = metric_shortest_cost


	end = time.time()
	report["elapsed_time"] = end - start

	print("Saving report in:",report_file_name)
	with open(report_file_name, 'w',encoding='utf-8') as f:
		json.dump(report, f,ensure_ascii=False,indent=2)



if __name__ == "__main__":
	main()
