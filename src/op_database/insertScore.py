'''
	Script responsible for inserting a player Score into the database
'''

from pathlib import Path
import sys
sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from src.op_database import operations
from src.op_database.connect import connect_database

from datetime import datetime
import pytz

def check_for_decreases(old,new):
	'''
		Warn user if a score decreased in certain season,
		compared to the last insertion
	'''
	for season in old["Seasons"]:
		total_old = old["Seasons"][season]["Total"]
		if season in new["Seasons"]:
			total_new = new["Seasons"][season]["Total"]
		else:
			total_new = -1
		if total_old > total_new:
			print("[WARNING - " + str(new["transfermarkt_id"]) + " ] Calculation: Score decreased in season " + str(season),end=': ')
			print(str(round(total_old,4)) + " > " + str(round(total_new,4)))

	return

def insert_score(idPlayer,transfermarkt_id,resultant_component):
	'''
		Construct the document to be inserted in SCORE collection
	'''
	collection_name = "SCORE"
	db = connect_database()

	transfermarkt_id = int(transfermarkt_id)

	oldDocument = operations.get_document_by_att(collection_name,transfermarkt_id,att="transfermarkt_id",db=db)

	resultant_component["Seasons"] = dict(sorted(resultant_component["Seasons"].items(), key=lambda x: x[0],reverse=True))
	resultant_component["Clubs"]["Seasons"] = dict(sorted(resultant_component["Clubs"]["Seasons"].items(), key=lambda x: x[0],reverse=True))
	resultant_component["NationalTeam"]["Seasons"] = dict(sorted(resultant_component["NationalTeam"]["Seasons"].items(), key=lambda x: x[0],reverse=True))
	
	document = {
		"_id":idPlayer,
		"transfermarkt_id":transfermarkt_id,
		"Total":resultant_component["Total"],
		"C1":resultant_component["C1"],
		"C2":resultant_component["C2"],
		"C3":resultant_component["C3"],
		"Seasons":resultant_component["Seasons"],
		"Clubs":resultant_component["Clubs"],
		"NationalTeam":resultant_component["NationalTeam"],
		"last_update": datetime.now(pytz.timezone('America/Sao_Paulo')),
	}
	if oldDocument is None:
		operations.insert_document(db,collection_name,document)
	else:
		check_for_decreases(oldDocument,document)
		operations.replace_document(db,collection_name,oldDocument,document)
	return