'''
	Deals with database connection
'''
import pymongo
import os
# import config

def connect_database():
	'''
		Connects with mongodb database
	'''
	exception = Exception("[DB ERROR]: Error connecting to database")
	try:
		CONNECTION_STRING = os.getenv('mongodb_uri')
		client = pymongo.MongoClient(CONNECTION_STRING)
		mongodb_db = os.getenv("mongodb_db")
		db = client[mongodb_db]
	except:
		raise exception
	return db