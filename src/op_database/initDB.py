'''
	Script responsible for initializing the CONTINENT, COUNTRY and CHAMPIONSHIP databases
	with data stored at /src/data/db
'''
import pymongo
import json
from pathlib import Path
import sys

from connect import connect_database
import operations

sys.path.append(str(Path(__file__).absolute().parent.parent))
from utils import dbUtils
from utils import utils

continentPath = dbUtils.dataPath + "continent.json"
countryPath = dbUtils.dataPath + "country.json"
championshipPath = dbUtils.dataPath + "championship.json"

def init_continent(db):
	'''
		Initialize CONTINENT collection
	'''
	exception = Exception("[DB ERROR]: Error initializing continent db")
	try:
		continentJson = json.load(open(continentPath,"r"))
		if isinstance(continentJson,list):
			continentJson = continentJson[0]
		for continent in continentJson:
			document = {
				"_id":continent,
				"club_weights":continentJson[continent]["club_weights"],
				"national_weights":continentJson[continent]["national_weights"]
			}
			operations.insert_document(db,"CONTINENT",document)
	except:
		raise exception
	return

def init_country(db):
	'''
		Initialize COUNTRY collection
	'''
	exception = Exception("[DB ERROR]: Error initializing country db")
	possibleContinents = db["CONTINENT"].distinct("_id")
	try:
		countryJson = json.load(open(countryPath,"r"))
		if isinstance(countryJson,list):
			countryJson = countryJson[0]
		for country in countryJson:
			continent = countryJson[country]["continent"]
			if continent not in possibleContinents: raise Exception("[DB ERROR]: Invalid continent (" + continent + ") for country: " + country)
			document = {
				"_id":country,
				"transfermarkt_team_id": countryJson[country]["transfermarkt_team_id"],
				"continent":continent,
				"weights":countryJson[country]["weights"],
			}
			operations.insert_document(db,"COUNTRY",document)
	except:
		raise exception
	return

def init_championship(db):
	'''
		Initialize CHAMPIONSHIP collection
	'''
	exception = Exception("[DB ERROR]: Error initializing championship db")
	possibleCountries = db["COUNTRY"].distinct("_id")
	possibleCountries.extend(utils.extraCountries)

	check_weight_champion = {}
	check_weight_runnerup = {}


	try:
		championshipJson = json.load(open(championshipPath,"r"))
		if isinstance(championshipJson,list):
			championshipJson = championshipJson[0]
		for championship in championshipJson:
			country = championshipJson[championship]["country"]
			if country not in possibleCountries: raise Exception("[DB ERROR]: Invalid country (" + country + ") for championship: " + championship)
			
			champion_name_list = []
			runnerup_name_list = []

			if isinstance(championshipJson[championship]["champion_name"],str):
				champion_name_list = [championshipJson[championship]["champion_name"]]
			else:
				for c in championshipJson[championship]["champion_name"]:
					champion_name_list.append(c)
			if isinstance(championshipJson[championship]["runnerup_name"],str):
				runnerup_name_list = [championshipJson[championship]["runnerup_name"]]
			else:
				for r in championshipJson[championship]["runnerup_name"]:
					runnerup_name_list.append(r)

			champion_name_list_sanitized = []
			runnerup_name_list_sanitized = []

			for c in champion_name_list:
				sanitized = utils.sanitize_championship(c)
				if sanitized not in check_weight_champion:
					check_weight_champion[sanitized] = championshipJson[championship]["champion_weight"]
				else:
					if check_weight_champion[sanitized] != championshipJson[championship]["champion_weight"]:
						raise Exception("Champion names with different weight:",championshipJson[championship]["name"])
				champion_name_list_sanitized.append(sanitized)
			for r in runnerup_name_list:
				sanitized = utils.sanitize_championship(r)
				if sanitized not in check_weight_runnerup:
					check_weight_runnerup[sanitized] = championshipJson[championship]["runnerup_weight"]
				else:
					if check_weight_runnerup[sanitized] != championshipJson[championship]["runnerup_weight"]:
						raise Exception("Runnerup names with different weight:",championshipJson[championship]["name"])
				runnerup_name_list_sanitized.append(sanitized)

			document = {
				"_id":str(championship).upper(),
				"name":championshipJson[championship]["name"],
				"country":country,
				"season_type":championshipJson[championship]["season_type"],
				"champion_name":champion_name_list,
				"champion_name_sanitized":champion_name_list_sanitized,
				"runnerup_name":runnerup_name_list,
				"runnerup_name_sanitized":runnerup_name_list_sanitized,
				"default_weight":championshipJson[championship]["default_weight"],
				"champion_weight":championshipJson[championship]["champion_weight"],
				"runnerup_weight":championshipJson[championship]["runnerup_weight"],
			}
			operations.insert_document(db,"CHAMPIONSHIP",document)
	except:
		raise exception
	return


def main():
	'''
		Initialize db collections
	'''
	db = connect_database()
	operations.delete_collection(db,"CONTINENT")
	init_continent(db)
	operations.delete_collection(db,"COUNTRY")
	init_country(db)
	operations.delete_collection(db,"CHAMPIONSHIP")	
	init_championship(db)

	db.PLAYER.create_index("transfermarkt_id",unique=True)
	db.SCORE.create_index("transfermarkt_id",unique=True)
	db.TEAM.create_index("transfermarkt_id",unique=True)
	db.COUNTRY.create_index("continent")
	db.CHAMPIONSHIP.create_index("champion_name_sanitized")
	db.CHAMPIONSHIP.create_index("runnerup_name_sanitized")

	return

if __name__ == "__main__":
	main()	