'''
	Implements an interface for basic database operations
'''

import pymongo
from pathlib import Path
import sys

sys.path.append(str(Path(__file__).absolute().parent.parent))
sys.path.append(str(Path(__file__).absolute().parent.parent.parent))
from src.op_database.connect import connect_database
from src.utils import dbUtils

def insert_document(db,collection_name,document):
	'''
		Inserts a document at a specific collection from the database
	'''
	exception = Exception("[DB ERROR]: Error inserting into " + collection_name + ":" + str(document))
	try:
		collection = dbUtils.check_collection_name(collection_name)
		db[collection].insert_one(document)
	except:
		raise exception
	return

def delete_document(db,collection_name,document):
	'''
		Deletes a document from a specific collection of the database
	'''
	exception = Exception("[DB ERROR]: Error deleting document from " + collection_name + ":" + str(document))
	try:
		collection = dbUtils.check_collection_name(collection_name)
		db[collection].delete_one(document)
	except:
		raise exception
	return

def delete_database(db):
	'''
		Deletes the whole database
	'''
	exception = Exception("[DB ERROR]: Error deleting database")
	try:
		collections = db.list_collection_names()
		for collection in collections:
			delete_collection(db,collection)
	except:
		raise exception
	return

def replace_document(db,collection_name,oldDocument,newDocument):
	'''
		Replace a document with another in a specific collection from the database
	'''
	exception = Exception("[DB ERROR]: Error replacing " + collection_name + ": " + str(oldDocument) + "->" + str(newDocument))
	try:
		collection = dbUtils.check_collection_name(collection_name)
		db[collection].replace_one(oldDocument,newDocument)
	except:
		raise exception
	return

def get_document_by_att(collection_name,value,att="_id",db=""):
	'''
		Retrieve a document by some attribute = value from a specific collection
	'''
	if db == "":
		db = connect_database()
	exception = Exception("[DB ERROR]: Error getting document from " + collection_name + " with " + att + " = " + str(value))
	try:
		collection = dbUtils.check_collection_name(collection_name)
		return db[collection].find_one({att:value})
	except:
		raise exception
	return

def delete_collection(db,collection_name):
	'''
		Deletes a collection from database
	'''
	exception = Exception("[DB ERROR]: Error deleting collection " + collection_name)
	try:
		collection = dbUtils.check_collection_name(collection_name)
		db[collection].delete_many({})
	except:
		raise exception
	return

def dump_collection(collection_name,db=""):
	'''
		Retrieve all elements from collection
	'''
	if db == "":
		db = connect_database()
	exception = Exception("[DB ERROR]: Error dumping collection " + collection_name)
	try:
		collection = dbUtils.check_collection_name(collection_name)
		return db[collection].find({})
	except:
		raise exception
	return			

def query_collection(collection_name,query,db=""):
	'''
		Run a specific query at a collection
		Ex: query = {"filter":{"_id":"test"},"projection":{"_id":0}}
	'''
	if db == "":
		db = connect_database()
	exception = Exception("[DB ERROR]: Error querying collection " + collection_name + ": " + str(query))
	try:
		collection = dbUtils.check_collection_name(collection_name)
		return db[collection].find(**query)
	except:
		raise exception
	return																												