'''
	Script responsible for inserting players and teams, using the
	extraction (from web) module implemented at /src/extraction
'''
from multiprocessing import Process
import pymongo
from pathlib import Path

from datetime import datetime
import pytz

import sys
sys.path.append(str(Path(__file__).absolute().parent.parent.parent))

from src.op_database.connect import connect_database
from src.op_database import operations

from src.extract import extractClub
from src.extract import extractPlayer

from src.graph_database.operations import update_edges_player


def alreadyIn(db,collection_name,key,value):
	'''
		True if collection already has a document with a given key
	'''
	query = list(db[collection_name].find({key:{ "$eq": value}}))
	if len(query) != 0:
		return query[0]	
	return ""

def insert_player(playerId,db="",debug=False):
	'''
		Inserting a player at the database
	'''
	if db == "":
		db = connect_database()
	player = extractPlayer.extractPlayer(int(playerId),debug=debug)
	fullExtraction = player.get_full_extraction()

	document = {
		"transfermarkt_id":playerId,
		"name":fullExtraction["Name"],
		"nationality":fullExtraction["Nationality"],
		"position":fullExtraction["Position"],
		"birth_date":fullExtraction["BirthDate"],
		"Teams":fullExtraction["Teams"],
		"Achievements":fullExtraction["Achievements"],
		"Performance":fullExtraction["Performance"],
		"last_update": datetime.now(pytz.timezone('America/Sao_Paulo')),
	}

	oldDocument = alreadyIn(db,"PLAYER","transfermarkt_id",playerId)

	if (oldDocument == ""):
		operations.insert_document(db,"PLAYER",document)
	else:
		document["_id"] = oldDocument["_id"]
		operations.replace_document(db,"PLAYER",oldDocument,document)

	update_edges_player(playerId)
	print("Player " + str(playerId) + " was successfully inserted")
	return

def insert_multiple_players(player_list,debug=False):
	'''
		Inserting multiple players using parallelization
	'''
	processes = []
	db = connect_database()
	for p in player_list:
		process = Process(target=insert_player,args=[p],kwargs={"db":db,"debug":debug})
		processes.append(process)
		process.start()
	for process in processes:
		if process.is_alive():
			process.join()	
	return

def insert_team(teamId,db="",debug=False):
	'''
		Inserting a team at the database
	'''
	if db == "":
		db = connect_database()
	teamId = int(teamId)
	team = extractClub.extractClub(teamId,debug=debug)
	fullExtraction = team.get_full_extraction()
	document = {
		# "_id":"",
		"transfermarkt_id":teamId,
		"name":fullExtraction["Name"],
		"country":fullExtraction["Country"],
		"Achievements":fullExtraction["Achievements"],
		"last_update": datetime.now(pytz.timezone('America/Sao_Paulo')),
	}

	oldDocument = alreadyIn(db,"TEAM","transfermarkt_id",teamId)
	if (oldDocument == ""):
		# nextId = len(list(db["TEAM"].find()))+1
		# document["_id"] = nextId
		operations.insert_document(db,"TEAM",document)
	else:
		document["_id"] = oldDocument["_id"]
		operations.replace_document(db,"TEAM",oldDocument,document)
	print("Team: " + str(teamId) + " was successfully inserted")
	return

def insert_team_check_status(teamId,db="",debug=False,callbackProgress=None):
	'''
		Insert team and check if it ended correctly
	'''
	worked = True
	try:
		insert_team(teamId,db,debug)
	except Exception as error:
		worked = False
		print("ERROR:",error)
	if callbackProgress is not None:
		if worked:
			callbackProgress(teamId,True)
		else:
			callbackProgress(teamId,False)
	return


def insert_multiple_teams(team_list,debug=False,callbackProgress=None):
	'''
		Inserting multiple teams using parallelization
	'''
	db = connect_database()
	processes = []
	for t in team_list:
		process = Process(target=insert_team_check_status,args=[t],kwargs={"db":db,"debug":debug,"callbackProgress":callbackProgress})
		processes.append(process)
		process.start()

	for process in processes:
		if process.is_alive():
			process.join()
	return


def remove_player(playerId,db="",debug=False):
	'''
		Remove a player from database
	'''
	if db == "":
		db = connect_database()

	oldDocument = alreadyIn(db,"PLAYER","transfermarkt_id",playerId)
	if oldDocument != "":
		operations.delete_document(db,"PLAYER",oldDocument)
	oldDocument = alreadyIn(db,"SCORE","transfermarkt_id",playerId)
	if oldDocument != "":
		operations.delete_document(db,"SCORE",oldDocument)
	return

def remove_team(teamId,db="",debug=False):
	'''
		Remove a team from database
	'''
	if db == "":
		db = connect_database()

	oldDocument = alreadyIn(db,"TEAM","transfermarkt_id",teamId)
	if oldDocument != "":
		operations.delete_document(db,"TEAM",oldDocument)
	return

def remove_player_check_status(playerId,db="",debug=False,callbackProgress=None):
	'''
		Remove a player and check if it ended correctly
	'''
	worked = True
	try:
		remove_player(playerId,db,debug)
	except Exception as error:
		worked = False
		print("ERROR:",error)
	if callbackProgress is not None:
		if worked:
			callbackProgress(playerId,True)
		else:
			callbackProgress(playerId,False)
	return

def remove_team_check_status(teamId,db="",debug=False,callbackProgress=None):
	'''
		Remove a team and check if it ended correctly
	'''
	worked = True
	try:
		remove_team(teamId,db,debug)
	except Exception as error:
		worked = False
		print("ERROR:",error)
	if callbackProgress is not None:
		if worked:
			callbackProgress(teamId,True)
		else:
			callbackProgress(teamId,False)
	return

def remove_multiple_players(player_list,debug=False,callbackProgress=None):
	'''
		Remove multiple players using parallelization
	'''
	db = connect_database()
	processes = []
	for p in player_list:
		process = Process(target=remove_player_check_status,args=[p],kwargs={"db":db,"debug":debug,"callbackProgress":callbackProgress})
		processes.append(process)
		process.start()

	for process in processes:
		if process.is_alive():
			process.join()
	return

def remove_multiple_teams(team_list,debug=False,callbackProgress=None):
	'''
		Remove multiple teams using parallelization
	'''
	db = connect_database()
	processes = []
	for t in team_list:
		process = Process(target=remove_team_check_status,args=[t],kwargs={"db":db,"debug":debug,"callbackProgress":callbackProgress})
		processes.append(process)
		process.start()

	for process in processes:
		if process.is_alive():
			process.join()
	return	