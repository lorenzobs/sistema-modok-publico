'''
	Utils for operational database
'''
from pathlib import Path
import sys

dataPath = str(Path(__file__).absolute().parent.parent.parent) + "/data/db/"

def check_collection_name(collection_name):
	'''
		Check if a collection name is valid
	'''
	exception = Exception("[DB ERROR]: " + collection_name + " is not a valid collection")
	try:
		collection_name = collection_name.upper()
		if collection_name not in ["CONTINENT","COUNTRY","CHAMPIONSHIP","PLAYER","TEAM","SCORE"]:	
			raise exception
	except:
		raise exception
	return collection_name


