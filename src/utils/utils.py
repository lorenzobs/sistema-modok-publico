'''
	Useful functions and data structures
'''

import datetime
import numpy
from unidecode import unidecode


currentYear = datetime.date.today().year

extraCountries = ["Continental","National Continental","Friendly","World","World National"]	


def convert_season(oldSeason):
	'''
		Unifies season formatting
	'''
	newSeason = ""
	# Case season -> 2020 (string)
	if isinstance(oldSeason,int) or isinstance(oldSeason,numpy.int64):
		newSeason = int(oldSeason)

	# Case season -> 2022 (string)
	elif len(oldSeason) == 4:
		newSeason = int(oldSeason)

	# Case season -> 21/22
	elif len(oldSeason) == 5 and "/" in oldSeason:
		firstYear = oldSeason.split("/")[0] 
		if "00" <= firstYear and firstYear <= str(currentYear)[2:]:
			newSeason = "20" + firstYear
		else:
			newSeason = "19" + firstYear
		newSeason = int(newSeason)

	# Case season -> 1902/03
	elif len(oldSeason) == 7:
		newSeason = int(oldSeason[0:4])

	# Case season -> 2003/2004
	elif len(oldSeason) == 9:
		newSeason = int(oldSeason[0:4])

	else:
		raise Exception("Invalid season type: " + oldSeason)

	return newSeason

def date_to_season(normalDate,typeOfSeason="North"):
	'''
		Receives a date in DD/MM/YYYY format
		and returns the football season at this date
	'''

	splitDate = normalDate.split("/")
	if len(splitDate) < 3:
		raise Exception("Invalid date: " + normalDate)
	month = int(splitDate[1])
	year = int(splitDate[2])
	if typeOfSeason == "North":
		if month <= 6:
			return year-1
		else:
			return year
	else:
		return year

def sanitize_championship(champ):
	return unidecode(champ.lower().replace(" ","").replace("-",""))

def query_champion(champ):
	'''
		Returns the query responsible for
		championship winner names 
	'''
	sanitized_champ = sanitize_championship(champ)
	query_champion = {"filter":{"champion_name_sanitized":{"$in":[sanitized_champ]}}}
	return query_champion

def query_runnerup(champ):
	'''
		Returns the query responsible for
		championship runner-up names 
	'''
	sanitized_champ = sanitize_championship(champ)
	query_runnerup = {"filter":{"runnerup_name_sanitized":{"$in":[sanitized_champ]}}}
	return query_runnerup
