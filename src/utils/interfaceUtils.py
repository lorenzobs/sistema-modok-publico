def convert_seconds(seconds):
	total_seconds = int(seconds)
	hours = 0
	minutes = 0
	seconds = 0
	if total_seconds < 60:
		seconds = total_seconds
	elif total_seconds >= 60 and total_seconds < 3600:
		minutes = int(total_seconds/60)
		seconds = total_seconds % 60
	else:
		hours = int(total_seconds/3600)
		minutes = int( (total_seconds % 3600)/60)
		seconds = (total_seconds % 3600) % 60
	return hours,minutes,seconds


def format_seconds(seconds):
	formatted = ""
	hours,minutes,seconds = convert_seconds(seconds)
	if hours != 0:
		formatted += str(hours) + "h"
	if minutes != 0:
		formatted += str(minutes) + "m"
	if seconds != 0:
		formatted += str(seconds) + "s"
	return formatted
