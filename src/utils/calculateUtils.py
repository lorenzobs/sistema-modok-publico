'''
	Useful functions and structures to the score calculation
'''
from pathlib import Path
import sys
import numpy as np

sys.path.append(str(Path(__file__).absolute().parent.parent.parent))
from src.op_database import operations
from src.utils import utils

# Weights for each individual attribute
stats_weights = {"goal":2,
				 "match":1,
				 "assist":1.5,
				 "hat-trick":3,
				 "gwsg":{"Goalkeeper":1.3,"Defender":0.65},
				 "savedPenalties":2.5}

# Approximate number of matches for each "type" of championship (represented by its default weight) needed
champs_min_matches = {
	"0.5":20,
	"0.67":20,
	"1.0":20,
	"1.2":1,
	"1.5":8,
	"1.75":1,
	"2.0":20,
	"2.87":7,
	"3.09":7,
	"3.325":1,
	"3.68":3,
	"3.92":1,
	"4.6":3,
	"4.75":1,
	"5.7":4,
	"5.74":1,
	"7.18":1,
	"11.41":4,
	"19.02":4,
}

exception_champs_weights = {
	"BCP1":{range(0,1971):1.8,range(1971,2004):1.65},
	"BCPF":{range(0,1971):1.8,range(1971,2004):1.65},
	"BRCG":{range(0,1971):1.8,range(1971,2004):1.65},
	"BRCF":{range(0,1971):1.8,range(1971,2004):1.65},
	"RIOSP":{range(0,1971):1.85},
	"BRRS":{range(0,1971):1.6},
	"RS2F":{range(0,1971):1.6},
	"BRCM":{range(0,1971):1.6},
	"BRPE":{range(0,1971):1.6,range(1971,2004):1.4},
	"BRPR":{range(0,1971):1.6,range(1971,2004):1.4},
	"BRCB":{range(0,1971):1.6,range(1971,2004):1.4},
	"BRSC":{range(0,1971):1.6,range(1971,2004):1.4},
	"BRCE":{range(0,1971):1.6,range(1971,2004):1.4},
	"OLN1":{range(0,1962):1.4},
	"OLW1":{range(0,1962):1.4},
	"OSW1":{range(0,1962):1.4},
	"OLS1":{range(0,1962):1.4},
	"OLSB":{range(0,1962):1.4},
	"L1":{range(0,1962):1.4},
}


def calculate_full_ppg(performance):
	'''
		Calculate usage of points in each season
	'''

	full_ppg = {"All":0}
	total_matches = 0
	total_points = 0
	for season in performance:
		if performance[season]["Matches"] == 0:
			full_ppg[season] = 0
		else:
			full_ppg[season] = performance[season]["Points"] / (3 * performance[season]["Matches"])
		total_matches += performance[season]["Matches"]
		total_points += performance[season]["Points"]
	if total_matches == 0:
		full_ppg["All"] = 0
	else:
		full_ppg["All"] = total_points/(3*total_matches)
	return full_ppg

def get_player_data(idPlayer,db=""):
	'''
		Returns the document of a player stored in the db
	'''
	document = operations.get_document_by_att("PLAYER",idPlayer,att="transfermarkt_id",db=db)
	return document

def get_teams_data(teams,idPlayer,db=""):
	'''
		Returns teams documents stored at a db and
		teams missing in the db
	'''
	toAddTeams = {}
	teams_ids = {}
	national_team_data = teams["NationalTeam"]
	teams_ids[national_team_data["TeamId"]] = {"name":national_team_data["TeamName"],"NationalTeam":True}
	for t in teams["Clubs"]:
		if t["TeamId"] == "":
			t["TeamId"] = -1
		teams_ids[t["TeamId"]] = {"name":t["TeamName"],"NationalTeam":False} 
	teams_data = {}
	for t in teams_ids.keys():
		document = operations.get_document_by_att("TEAM",t,att="transfermarkt_id",db=db)
		if document == None:
			toAddTeams[t] = teams_ids[t] 
			toAddTeams[t]["TriggerPlayer"] = idPlayer
			continue
		teams_data[t] = document
		teams_data[t]["NationalTeam"] = teams_ids[t]["NationalTeam"]
	return teams_data,toAddTeams

def mount_champ_weight_struct(champ_id,champ,weight="",type="",country="",default_weight="",season=""):
	if champ_id is None:
		champ = None
		weight = 0
		type = None
		country = None
		default_weight = 0
		season = None
	if champ_id in exception_champs_weights:
		for rng in exception_champs_weights[champ_id]:
			if int(season) in rng:
				if type == "Id":
					weight = exception_champs_weights[champ_id][rng]
				elif type == "Champion":
					weight = exception_champs_weights[champ_id][rng] * 10
	return {"champ_id":champ_id,
			"champ":champ,
			"weight":weight,
			"type":type,
			"country":country,
			"default_weight":default_weight}

def find_champ_weight(champ,season="",db=""):
	'''
		Receives a championship and returns its weight
	'''
	queries = get_queries(champ)
	for q in queries:
		query = q["query"]
		response = operations.query_collection("CHAMPIONSHIP",query,db=db)
		count = len(list(response.clone()))
		if (count == 1):
			return mount_champ_weight_struct(response[0]["_id"],champ,weight=response[0][q["weight_name"]],type=q["type"],country=response[0]["country"],default_weight=response[0]["default_weight"],season=season)
		elif (count > 1): 
			weight = response[0][q["weight_name"]]
			default_weight = response[0]["default_weight"]
			country = response[0]["country"]
			champ_id = response[0]["_id"]
			for r in response:
				if r[q["weight_name"]] != weight or r["country"] != country:
					raise Exception("[ERROR]: More than one champ for: " + champ)
			return mount_champ_weight_struct(champ_id,champ,weight=weight,type=q["type"],country=country,default_weight=default_weight,season=season)
	return mount_champ_weight_struct(None,champ)

def find_country_season_weight(country,season,db=""):
	'''
		Receives a country-season and returns its weight
	'''
	return0 = {"country":country,"weight":0,"season":season,"continent":None}
	if country in utils.extraCountries:
		return {"country":country,"weight":1,"season":season,"continent":None}

	query = {"filter":{"_id":country}}
	response = operations.query_collection("COUNTRY",query,db=db)
	count = len(list(response.clone()))
	if (count == 1):
		if str(season) not in response[0]["weights"]:
			# print("[WARNING] - Weight " + country + " missing weight for season " + str(season),file=sys.stderr)
			mean = round(np.array(list(response[0]["weights"].values())).mean(),2)
			return {"country":country,"season":season,"weight":mean,"continent":response[0]["continent"]}
		return {"country":country,"season":season,"weight":response[0]["weights"][str(season)],"continent":response[0]["continent"]}
	elif (count > 1):
		raise Exception("[ERROR]: More than one country for: " + str(country))
	else:
		raise Exception("[ERROR]: Could not find country: " + str(country))

	return

def find_continent_season_weight(continent,season,country,db=""):
	'''
		Receives a continent-season and returns its weight
	'''
	if country in ["Friendly","World","World National"]:
		return {"continent":continent,"season":season,"key":None,"weight":1}
	elif country == "National Continental":
		key = "national_weights"
	else:
		key = "club_weights"
	return0 = {"continent":continent,"season":season,"key":None,"weight":0}

	query = {"filter":{"_id":continent}}
	response = operations.query_collection("CONTINENT",query,db=db)
	count = len(list(response.clone()))
	if (count == 1):
		if str(season) not in response[0][key]:
			# print("[WARNING] - Weight: " + str(continent) + " missing weight for " + str(key) + " season " + str(season),file=sys.stderr)
			mean = round(np.array(list(response[0][key].values())).mean(),2)
			return {"continent":continent,"season":season,"key":key,"weight":mean}
		return {"continent":continent,"season":season,"key":key,"weight":response[0][key][str(season)]}
	elif (count > 1): raise Exception("[ERROR]: More than one continent for: " + str(continent))
	else: raise Exception("[ERROR]: Could not find continent: " + str(continent))
	return

def find_continent_from_team(transfermarkt_team_id,db=""):
	'''
		Receives a team id and returns the team's continent
	'''
	country = find_country_from_team(transfermarkt_team_id,db=db)
	query = {"filter":{"_id":country}}
	response = operations.query_collection("COUNTRY",query,db=db)
	continent = response[0]["continent"]
	return continent

def find_country_from_team(transfermarkt_team_id,db=""):
	'''
		Receives a team id and returns the team's country
	'''
	query = {"filter":{"transfermarkt_id":int(transfermarkt_team_id)}}
	response = operations.query_collection("TEAM",query,db=db)
	country = response[0]["country"]
	return country 

def find_possible_champ_ids(champ):
	'''
		Find all champ ids that have champ as _id, champion_name or runnerup_name
	'''
	possible = []
	queries = get_queries(champ)
	for q in queries:
		query = q["query"]
		response = operations.query_collection("CHAMPIONSHIP",query)
		for r in response:
			possible.append(r["_id"])
	return possible

def get_queries(champ):
	'''
		Get search queries for championships
	'''
	queries = [
		{"type":"Id","query":{"filter":{"_id":str(champ).upper()}},"weight_name":"default_weight"},
		{"type":"Champion","query":utils.query_champion(champ),"weight_name":"champion_weight"},
		{"type":"Runner-up","query":utils.query_runnerup(champ),"weight_name":"runnerup_weight"}
	]
	return queries


def find_first_season_team(team,season,teamsHePlayedBySeason,isNationalTeam=False):
	firstSeason = season

	teamsHePlayedBySeasonSorted = dict(sorted(teamsHePlayedBySeason["Seasons"].items(), key=lambda x:int(x[0])))
	teamsHePlayedBySeasonReversed = dict(sorted(teamsHePlayedBySeason["Seasons"].items(), key=lambda x:int(x[0]),reverse=True))

	if isNationalTeam == True:
		for season in teamsHePlayedBySeasonSorted:
			if team in teamsHePlayedBySeasonSorted[season]:
				return season
	else:
		keysList = list(teamsHePlayedBySeasonReversed.keys())
		curIdx = 0
		for i in range(0,len(keysList)):
			if int(keysList[i]) == int(season):
				curIdx = i
				break
		for i in range(curIdx,len(keysList)):
			if team in teamsHePlayedBySeasonReversed[keysList[i]]:
				firstSeason = keysList[i]
			else:
				break
	return firstSeason

def find_last_season_team(team,season,teamsHePlayedBySeason,isNationalTeam=False):
	lastSeason = season

	teamsHePlayedBySeasonSorted = dict(sorted(teamsHePlayedBySeason["Seasons"].items(), key=lambda x:int(x[0])))
	teamsHePlayedBySeasonReversed = dict(sorted(teamsHePlayedBySeason["Seasons"].items(), key=lambda x:int(x[0]),reverse=True))

	if isNationalTeam == True:
		for season in teamsHePlayedBySeasonReversed:
			if team in teamsHePlayedBySeasonReversed[season]:
				return season
	else:
		keysList = list(teamsHePlayedBySeasonSorted.keys())
		curIdx = 0
		for i in range(0,len(keysList)):
			if int(keysList[i]) == int(season):
				curIdx = i
				break
		for i in range(curIdx,len(keysList)):
			if team in teamsHePlayedBySeasonSorted[keysList[i]]:
				lastSeason = keysList[i]
			else:
				break
	return lastSeason